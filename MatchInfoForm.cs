﻿using System.Collections.Generic;
using System.Windows.Forms;
using BrightIdeasSoftware;
using System.Linq;

namespace MicrobialInSilicoTyper
{
    public partial class MatchInfoForm : Form
    {
        private readonly ContigCollection _cc;
        private readonly FormCollection _frms;
        private readonly InSilicoTyping _ist;
        private readonly Dictionary<string, HashSet<Marker>> _tests = new Dictionary<string, HashSet<Marker>>();
        private readonly TypingResultsCollection _typingResults;


        public MatchInfoForm(FormCollection frms,
                             InSilicoTyping ist,
                             ContigCollection cc,
                             TypingResultsCollection typingResults)
        {
            _frms = frms;
            _ist = ist;
            _cc = cc;
            _typingResults = typingResults;

            InitializeComponent();

            //set up dict of tests and corresponding Marker
            foreach (Marker marker in _ist.Markers)
            {
                if (_tests.ContainsKey(marker.TestName))
                {
                    _tests[marker.TestName].Add(marker);
                }
                else
                {
                    _tests.Add(marker.TestName, new HashSet<Marker> {marker});
                }
            }

            //setup OLVs
            SetupMatchInfoOLV();
            SetupStrainsOLV();
            SetupTestsOLV();
            SetupMarkersOLV();

            //setup initial set of marker matches to show
            if (_cc == null) return;
            var l = new List<MarkerMatch>();
            l.AddRange(_cc.MarkerMatches);

            olvMatchInfo.SetObjects(l);

            olvStrains.SelectObject(cc);
        }

        private void SetupMatchInfoOLV()
        {
            olvMatchInfo.UseTranslucentSelection = true;
            olvMatchInfo.UseAlternatingBackColors = true;
            olvMatchInfo.FullRowSelect = true;
            olvMatchInfo.OwnerDraw = true;
            olvMatchInfo.GridLines = true;

            olvMatchInfo.Columns.Add(new OLVColumn("Strain", "StrainName"));
            olvMatchInfo.Columns.Add(new OLVColumn("marker", "MarkerName"));
            olvMatchInfo.Columns.Add(new OLVColumn("Test", "TestName"));
            olvMatchInfo.Columns.Add(new OLVColumn("Result", "MarkerCall"));
            olvMatchInfo.Columns.Add(new OLVColumn("Subject Match", "ContigMatchName"));

            olvMatchInfo.Columns.Add(new OLVColumn("Query Match", "AlleleMatch"));

            olvMatchInfo.Columns.Add(new OLVColumn
            {
                Text = "Metadata",
                AspectGetter = o =>
                {
                    var match = (MarkerMatch)o;
                    string alleleMatch = match.AlleleMatch;
                    if (alleleMatch == null)
                        return null;

                    string testName = match.Marker.TestName;
                    ExtraTestInfo extraInfo =
                        _typingResults.TestExtraInfoDict[testName];
                    if (extraInfo == null) return null;
                    List<string> headers =
                        extraInfo.GetExtraInfoHeaders();

                    ContigCollection contigCollection =
                        match.Contig.MultifastaFile;

                    var matches = new List<MarkerMatch>();
                    foreach (
                        Marker marker in
                            _typingResults.TestMarkerDict[testName])
                    {
                        MarkerMatch markerMatch =
                            contigCollection.MarkerMatchesDict[marker];
                        matches.Add(markerMatch);
                    }


                    List<string> list =
                        _typingResults.TestExtraInfoDict[testName].
                            GetExtraInfo(matches, true);

                    if (list.Count == 0)
                    {
                        return null;
                    }
                    var metadata = new List<string>();
                    for (int i = 0; i < headers.Count; i++)
                    {
                        metadata.Add(string.Format("{0}={1}",
                                                   headers[i],
                                                   list[i]));
                    }
                    if (metadata.Count == 1)
                    {
                        return metadata[0];
                    }

                    return string.Join("::", metadata);
                }
            });

            olvMatchInfo.Columns.Add(new OLVColumn("Match Found?", "CorrectMarkerMatch"));
            olvMatchInfo.Columns.Add(new OLVColumn
            {
                Text = "Null Match",
                AspectGetter = o =>
                {
                    var match = (MarkerMatch)o;
                    return string.IsNullOrEmpty(match.Amplicon);
                }
            });
            olvMatchInfo.Columns.Add(new OLVColumn
            {
                Text = "Contig Truncation?",
                AspectGetter = o =>
                {
                    var match = (MarkerMatch)o;
                    return match.IsContigTruncation;
                }
            });
            olvMatchInfo.Columns.Add(new OLVColumn
            {
                Text = "Mismatches",
                AspectGetter = o =>
                {
                    var match = (MarkerMatch)o;
                    if (string.IsNullOrEmpty(match.Amplicon))
                        return "";
                    return match.Mismatches;
                }
            });
            olvMatchInfo.Columns.Add(new OLVColumn
            {
                Text = "Blast Mismatches",
                AspectGetter = o =>
                {
                    var match = (MarkerMatch)o;
                    if (string.IsNullOrEmpty(match.Amplicon))
                        return string.Empty;
                    if (match.BlastResults == null)
                        return string.Empty;
                    return match.BlastResults.Mismatches;
                }
            });
            olvMatchInfo.Columns.Add(new OLVColumn
            {
                Text = "Blast Gaps",
                AspectGetter = o =>
                {
                    var match = (MarkerMatch)o;
                    if (string.IsNullOrEmpty(match.Amplicon))
                        return string.Empty;
                    if (match.BlastResults == null)
                        return string.Empty;
                    return match.BlastResults.Gaps;
                }
            });
            olvMatchInfo.Columns.Add(new OLVColumn
            {
                Text = "%ID",
                AspectGetter = o =>
                {
                    var match = (MarkerMatch)o;
                    if (string.IsNullOrEmpty(match.Amplicon))
                        return "";
                    return match.BlastPercentIdentity;
                }
            });
            olvMatchInfo.Columns.Add(new OLVColumn
            {
                Text = "Alignment Length",
                AspectGetter = o =>
                {
                    var match = (MarkerMatch)o;
                    if (string.IsNullOrEmpty(match.Amplicon))
                        return "";
                    return match.BlastAlignmentLength;
                }
            });
            olvMatchInfo.Columns.Add(new OLVColumn
            {
                Text = "Amplicon Length",
                AspectGetter = o =>
                {
                    var match = (MarkerMatch)o;
                    if (string.IsNullOrEmpty(match.Amplicon))
                        return "";
                    return match.AmpliconSize;
                }
            });
            olvMatchInfo.Columns.Add(new OLVColumn("Expected Amplicon Length", "ExpectedAmpliconSize"));
            olvMatchInfo.Columns.Add(new OLVColumn
            {
                Text = "Subject Start Index",
                AspectGetter = o =>
                {
                    var match = (MarkerMatch)o;
                    if (string.IsNullOrEmpty(match.Amplicon))
                        return "";
                    return match.StartIndex;
                }
            });
            olvMatchInfo.Columns.Add(new OLVColumn
            {
                Text = "Subject End Index",
                AspectGetter = o =>
                {
                    var match = (MarkerMatch)o;
                    if (string.IsNullOrEmpty(match.Amplicon))
                        return "";
                    return match.EndIndex;
                }
            });
            olvMatchInfo.Columns.Add(new OLVColumn("Amplicon", "Amplicon"));
            olvMatchInfo.Columns.Add(new OLVColumn
            {
                Text = "Query Sequence",
                AspectGetter = o =>
                {
                    var match = (MarkerMatch)o;
                    if (match.BlastResults == null)
                        return string.Empty;
                    return match.BlastResults.QueryAln;
                }
            });
            olvMatchInfo.Columns.Add(new OLVColumn
            {
                Text = "Orientation",
                AspectGetter = o =>
                {
                    var match = (MarkerMatch)o;
                    if (match.BlastResults == null)
                        return null;
                    return match.BlastResults.ReverseComplement
                               ? "-"
                               : "+";
                }
            });
            olvMatchInfo.Columns.Add(new OLVColumn
            {
                Text = "Amplicon in genome orientation",
                AspectGetter = o =>
                {
                    var match = (MarkerMatch)o;
                    if (match.BlastResults == null)
                        return null;
                    return match.BlastResults.ReverseComplement
                               ? Misc.ReverseComplement(match.Amplicon)
                               : match.Amplicon;
                }
            });

            olvMatchInfo.Columns.Add(new OLVColumn
            {
                Text = "Forward Primer",
                AspectGetter = o =>
                {
                    var match = (MarkerMatch)o;
                    Marker marker = match.Marker;
                    if (marker.TypingTest == TestType.PCR ||
                        marker.TypingTest == TestType.Repeat)
                    {
                        bool b = match.ForwardPrimerRevComp;
                        return b ? "RevComp" : "";
                    }
                    return null;
                }
            });
            olvMatchInfo.Columns.Add(new OLVColumn
            {
                Text = "Reverse Primer",
                AspectGetter = o =>
                {
                    var match = (MarkerMatch)o;
                    Marker marker = match.Marker;
                    if (marker.TypingTest == TestType.PCR ||
                        marker.TypingTest == TestType.Repeat)
                    {
                        bool b = match.ReversePrimerRevComp;
                        return b ? "RevComp" : "";
                    }
                    return null;
                }
            });
        }

        private void SetupStrainsOLV()
        {
            olvStrains.UseTranslucentSelection = true;
            olvStrains.UseAlternatingBackColors = true;
            olvStrains.FullRowSelect = true;
            olvStrains.OwnerDraw = true;
            olvStrains.GridLines = true;

            olvStrains.Columns.Add(new OLVColumn("Strain", "Name")
                                       {
                                           FillsFreeSpace = true,
                                       });

            olvStrains.SetObjects(_ist.MultifastaFileDict.Select(p => p.Value));

            olvStrains.MouseClick += (sender, args) => UpdateMatchInfoOnRightClick(args);
        }

        private void SetupTestsOLV()
        {
            olvTests.UseTranslucentSelection = true;
            olvTests.UseAlternatingBackColors = true;
            olvTests.FullRowSelect = true;
            olvTests.OwnerDraw = true;
            olvTests.GridLines = true;

            olvTests.Columns.Add(new OLVColumn
                                     {
                                         Text = "Test",
                                         FillsFreeSpace = true,
                                         AspectGetter = o =>
                                                            {
                                                                var testMarkers =
                                                                    (KeyValuePair<string, HashSet<Marker>>) o;
                                                                return testMarkers.Key;
                                                            }
                                     });

            olvTests.SetObjects(_tests);
            olvTests.SelectAll();

            olvTests.SelectionChanged += (sender, args) =>
                                             {
                                                 var hash = new HashSet<Marker>();
                                                 foreach (object objTest in olvTests.SelectedObjects)
                                                 {
                                                     var testMarkers = (KeyValuePair<string, HashSet<Marker>>) objTest;
                                                     foreach (Marker marker in testMarkers.Value)
                                                     {
                                                         hash.Add(marker);
                                                     }
                                                 }

                                                 olvMarkers.SetObjects(hash);
                                                 olvMarkers.SelectAll();
                                             };
            olvTests.MouseClick += (sender, args) => UpdateMatchInfoOnRightClick(args);
        }

        private void SetupMarkersOLV()
        {
            olvMarkers.UseTranslucentSelection = true;
            olvMarkers.UseAlternatingBackColors = true;
            olvMarkers.FullRowSelect = true;
            olvMarkers.OwnerDraw = true;
            olvMarkers.GridLines = true;

            olvMarkers.Columns.Add(new OLVColumn("marker", "Name")
                                       {
                                           FillsFreeSpace = true,
                                       });

            olvMarkers.MouseClick += (sender, args) => UpdateMatchInfoOnRightClick(args);

            var hash = new HashSet<Marker>();
            foreach (object objTest in olvTests.SelectedObjects)
            {
                var testMarkers = (KeyValuePair<string, HashSet<Marker>>) objTest;
                foreach (Marker marker in testMarkers.Value)
                {
                    hash.Add(marker);
                }
            }

            olvMarkers.SetObjects(hash);
            olvMarkers.SelectAll();
        }

        private void UpdateMatchInfoOnRightClick(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
                UpdateMatchInfoOLV();
        }

        private void UpdateMatchInfoOLV()
        {
            var listMatches = new List<MarkerMatch>();
            foreach (object objStrains in olvStrains.SelectedObjects)
            {
                var strain = (ContigCollection) objStrains;
                foreach (object objMarker in olvMarkers.SelectedObjects)
                {
                    var marker = (Marker) objMarker;
                    MarkerMatch match;
                    if (!strain.MarkerMatchesDict.TryGetValue(marker, out match))
                        continue;
                    listMatches.Add(match);
                }
            }

            olvMatchInfo.SetObjects(listMatches);
        }
    }
}