﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;

namespace MicrobialInSilicoTyper
{
    public class InSilicoTyping
    {
        private readonly List<Marker> _markers = new List<Marker>();

        private Dictionary<string, ContigCollection> _multifastaFileDict =
            new Dictionary<string, ContigCollection>();


        private readonly List<FileInfo> _testInfoFiles;
        private CountdownEvent _countdown;

        //Constructor
        /// <summary>Start a new in silico analysis with a list of test info files.</summary>
        /// <param name="testInfoFiles">Files containing in silico typing test information</param>
        public InSilicoTyping(IEnumerable<FileInfo> testInfoFiles)
        {
            if (testInfoFiles == null) return;
            _testInfoFiles = testInfoFiles.ToList();
            ReadTestInfoFiles();
        }

        public Dictionary<string, ContigCollection> MultifastaFileDict
        {
            get { return _multifastaFileDict; }
        }

        public List<Marker> Markers
        {
            get { return _markers; }
        }

        public void AddGenomes(IEnumerable<string> files)
        {
            _multifastaFileDict = _multifastaFileDict
                .Union(files.Where(f => !_multifastaFileDict.ContainsKey(f)).ToDictionary(f => f, f => new ContigCollection(this, f)))
                .ToDictionary(f => f.Key, f => f.Value);
        }

        public void Run(BackgroundWorker bgw)
        {
            _countdown = new CountdownEvent(1);
            foreach (var p in _multifastaFileDict)
            {
                if (bgw.CancellationPending)
                {
                    break;
                }
                ContigCollection cc = p.Value;
                _countdown.AddCount();
                ThreadPool.QueueUserWorkItem(delegate
                                                 {
                                                     cc.GetMarkerMatchesSingleCore();
                                                     bgw.ReportProgress(1);
                                                     _countdown.Signal();
                                                 });
            }

            _countdown.Signal();
            _countdown.Wait();
        }


        /// <summary>Read typing test info from files.</summary>
        private void ReadTestInfoFiles()
        {
            foreach (FileInfo filename in _testInfoFiles)
            {
                ReadTestInfoFile(filename);
            }
        }

        /// <summary>Read typing test info from file.</summary>
        /// <param name="filename">Test info file.</param>
        private void ReadTestInfoFile(FileInfo filename)
        {
            using (var sr = new StreamReader(filename.FullName))
            {
                bool noError = true;
                if (filename.DirectoryName != null)
                {
                    var dir = new DirectoryInfo(filename.DirectoryName);

                    FileInfo[] allFiles = dir.GetFiles("*", SearchOption.AllDirectories);

                    var dict = new Dictionary<string, string>();
                    foreach (FileInfo fileInfo in allFiles)
                    {
                        string name = fileInfo.Name;
                        if (!dict.ContainsKey(name))
                        {
                            dict.Add(name, fileInfo.FullName);
                        }
                    }


                    var lPrimers = new List<Marker>();
                    try
                    {
                        // All test types                                                           || binary/allelic|| binary       || allelic only              || repeat only
                        //marker Name || Test Name || Test Type || Forward Primer || Reverse Primer || Amplicon Size || Range Factor || Allelic Database Filename || Repeat Size 
                        // 0          ||     1     ||     2     ||      3         ||        4       ||       5       ||     6        ||             7             ||    8
                        sr.ReadLine(); //skip first line
                        while (!sr.EndOfStream)
                        {
                            string readLine = sr.ReadLine();
                            if (readLine == null) continue;
                            string[] tmp = readLine.Split('\t');
                            int i, j;
                            double d;

                            string alleleFile = tmp[7];
                            string alleleFullPath = "";

                            if (dict.ContainsKey(alleleFile))
                            {
                                alleleFullPath = dict[alleleFile];
                            }


                            lPrimers.Add(new Marker(
                                             tmp[0],
                                             tmp[1],
                                             (TestType) (int.Parse(tmp[2])),
                                             tmp[3],
                                             tmp[4],
                                             int.TryParse(tmp[5], out i) ? i : -1,
                                             double.TryParse(tmp[6], NumberStyles.Any, CultureInfo.InvariantCulture,
                                                             out d)
                                                 ? d
                                                 : -1,
                                             alleleFullPath,
                                             int.TryParse(tmp[8], out j) ? j : -1
                                             ));
                        }
                    }
                    catch (Exception ex)
                    {
                        noError = false;
                    }
                    finally
                    {
                        if (noError)
                            _markers.AddRange(lPrimers);
                    }
                }
            }
        }

        /// <summary>Write the marker match results to a file.</summary>
        /// <param name="filename">Save filename.</param>
        /// <param name="verbose">Verbose or sparse results file.</param>
        /// <returns>Exception if error encountered.</returns>
        public void WriteResults(string filename, bool verbose)
        {
            var typingResults = new TypingResultsCollection(this, verbose);
            using (var sw = new StreamWriter(filename))
            {
                var headers = new List<string> {"Sample"}; //headers in the first line of the file
                //get all of the headers for each marker for each test including extra info each test
                foreach (var pair in typingResults.TestMarkerDict)
                {
                    string testName = pair.Key;
                    var markers = new List<Marker>(pair.Value);
                    markers.Sort();
                    foreach (Marker marker in markers)
                    {
                        headers.Add(marker.Name);
                    }

                    if (typingResults.TestExtraInfoDict[testName] == null)
                        continue;
                    List<string> extraInfoHeaders = typingResults.TestExtraInfoDict[testName].GetExtraInfoHeaders();
                    foreach (string extraInfoHeader in extraInfoHeaders)
                    {
                        headers.Add(extraInfoHeader);
                    }
                }
                sw.WriteLine(string.Join("\t", headers));
                //write each line of in silico typing data
                foreach (TypingResults typingResult in typingResults.Results)
                {
                    ContigCollection cc = typingResult.ContigCollection;
                    var line = new List<string> {cc.Name};
                    foreach (var pair in typingResults.TestMarkerDict)
                    {
                        string testName = pair.Key;
                        var markers = new List<Marker>(pair.Value);
                        //sort the Marker by Name
                        markers.Sort();
                        var markerMatches = new List<MarkerMatch>();
                        foreach (Marker marker in markers)
                        {
                            //for the current marker, get the marker match data
                            MarkerMatch markerMatch = cc.MarkerMatchesDict[marker];
                            markerMatches.Add(markerMatch);
                            line.Add(GetMarkerMatchData(marker, markerMatch, verbose));
                        }
                        //check if there is extra info for the current test
                        if (typingResults.TestExtraInfoDict[testName] == null)
                            continue;
                        //get the extra info
                        List<string> list = typingResults.TestExtraInfoDict[testName].GetExtraInfo(markerMatches,
                                                                                                   verbose);
                        foreach (string extraInfo in list)
                        {
                            //add it to the line
                            line.Add(extraInfo);
                        }
                    }
                    sw.WriteLine(string.Join("\t", line));
                }
            }
        }

        /// <summary>Get the marker match data (verbose or sparse) for a particular marker match.</summary>
        /// <param name="marker">marker.</param>
        /// <param name="mm">marker match.</param>
        /// <param name="verbose">Verbose or sparse data to be returned.</param>
        /// <returns>marker match data string.</returns>
        private static string GetMarkerMatchData(Marker marker, MarkerMatch mm, bool verbose)
        {
            switch (marker.TypingTest)
            {
                case TestType.AmpliconProbe:
                case TestType.OligoProbe:
                case TestType.PCR:
                    if (mm != null)
                    {
                        return mm.MarkerCall == "Present" ? "1" : "0";
                    }
                    return ("0");
                case TestType.Allelic:
                    if (mm != null)
                    {
                        if (verbose)
                            return (mm.CorrectMarkerMatch
                                        ? mm.MarkerCall
                                        : mm.AlleleMatch + "; " + mm.Mismatches + " mismatches");
                        return mm.CorrectMarkerMatch ? mm.MarkerCall : "";
                    }
                    return "";
                case TestType.Repeat:
                    return mm != null ? mm.MarkerCall : "";
                case TestType.SNP:
                    return mm != null ? mm.MarkerCall : "";
            }
            return null;
        }

        #region ExtraInfo

        private readonly List<ExtraTestInfo> _extraInfo = new List<ExtraTestInfo>();

        public IEnumerable<ExtraTestInfo> ExtraInfo
        {
            get { return _extraInfo; }
        }

        public void AddExtraInfo(ExtraTestInfo extraInfo)
        {
            _extraInfo.Add(extraInfo);
        }

        #endregion
    }
}