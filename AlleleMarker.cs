using System;
using System.IO;

namespace MicrobialInSilicoTyper
{
    [Serializable]
    public class AlleleMarker : IMarker
    {
        public string Name { get; set; }



        public string TestName { get; set; }

        private TestType _testType;

        public TestType ResultType
        {
            get { return _testType; }
            set { _testType = value; }
        }

        private string _alleleFilePath;
        public string AlleleFilePath
        {
            get
            {
                return _alleleFilePath;
            }
            set
            {
                if (File.Exists(value))
                {
                    _alleleFilePath = value;
                }
            }
        }

        private readonly DirectoryInfo _packageAllelesDir;

        public AlleleMarker(
            string testName,
            string name,
            string alleleFilePath,
            DirectoryInfo packageAllelesDir
            )
        {
            _testType = TestType.Allelic;
            TestName = testName;
            Name = name;
            _alleleFilePath = alleleFilePath;
            _packageAllelesDir = packageAllelesDir;
            CreateCopyOfAlleleFile();
        }

        public Marker GetMarker()
        {
            var p = new Marker(
                name: Name, 
                testName: TestName, 
                testType: TestType.Allelic, 
                fprimer: string.Empty, rprimer: string.Empty, 
                ampliconSize: -1, 
                ampliconRange: 0, 
                allelicDatabaseFilename: (new FileInfo(_alleleFilePath)).Name, 
                repeatSize: 0
                );
            return p;
        }

        /// <summary>Create a copy of the allele multifasta file in the alleles folder in the package folder.</summary>
        public void CreateCopyOfAlleleFile()
        {
            var fi = new FileInfo(_alleleFilePath);

            string destPath = Path.Combine(_packageAllelesDir.FullName, "alleles", fi.Name);

            if (File.Exists(destPath)) return;
            fi.CopyTo(destPath);
            _alleleFilePath = destPath;
        }
    }
}