﻿using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace MicrobialInSilicoTyper
{
    public class TypingResults
    {
        private readonly TypingResultsCollection _resultsCollection;
        private readonly bool _showFuzzyMatching;
        private readonly ContigCollection _contigCollection;
        private readonly Dictionary<string, List<MarkerMatch>> _testMatchDict = new Dictionary<string, List<MarkerMatch>>();
        private readonly Dictionary<string, Dictionary<Marker, MarkerMatch>> _testMarkerMatchDict = new Dictionary<string, Dictionary<Marker, MarkerMatch>>(); 
        private readonly Dictionary<string, List<string>> _testExtraInfoDict = new Dictionary<string, List<string>>(); 

        public TypingResults(ContigCollection contigCollection, TypingResultsCollection resultsCollection, bool showFuzzyMatching)
        {
            _contigCollection = contigCollection;
            _resultsCollection = resultsCollection;
            _showFuzzyMatching = showFuzzyMatching;

            foreach (MarkerMatch match in _contigCollection.MarkerMatches)
            {
                var testName = match.TestName;
                if (_testMatchDict.ContainsKey(testName))
                {
                    _testMatchDict[testName].Add(match);
                }
                else
                {
                    if (_resultsCollection.TestExtraInfoDict.ContainsKey(testName) && _resultsCollection.TestExtraInfoDict[testName] != null)
                    {
                        _testExtraInfoDict.Add(testName, _resultsCollection.TestExtraInfoDict[testName].GetExtraInfo(_contigCollection.MarkerMatches, _showFuzzyMatching));
                    }
                    _testMatchDict.Add(testName, new List<MarkerMatch>{match});
                }
                var marker = match.Marker;
                if (_testMarkerMatchDict.ContainsKey(testName))
                {
                    if (!_testMarkerMatchDict[testName].ContainsKey(marker))
                    {
                        _testMarkerMatchDict[testName].Add(marker, match);   
                    }
                }
                else
                {
                    _testMarkerMatchDict.Add(testName, new Dictionary<Marker, MarkerMatch>{{marker, match}});
                }
            }
        }

        public string GetMarkerResult(string testName, Marker marker, bool showFuzzyMatches)
        {
            Dictionary<Marker, MarkerMatch> dict;
            if (_testMarkerMatchDict.TryGetValue(testName, out dict))
            {
                MarkerMatch markerMatch;
                return dict.TryGetValue(marker, out markerMatch) ?
                    ((showFuzzyMatches) ? 
                        Misc.NumberRegex.Match(markerMatch.AlleleMatch).Value : 
                        markerMatch.MarkerCall) :
                    "null_marker_match";
            }
            return "null_test";
        }

        public string GetExtraInfo(string testName, int index)
        {
            List<string> list;
            if(_testExtraInfoDict.TryGetValue(testName, out list))
            {
                if (list.Count == 0)
                    return null;
                return list[index];
            }
            return null;
        }

        public string GetBinaryString(string testName)
        {
            var matches = _testMatchDict[testName];

            var dict = new Dictionary<Marker, MarkerMatch>();
            foreach (MarkerMatch markerMatch in matches)
            {
                if (!dict.ContainsKey(markerMatch.Marker))
                {
                    dict.Add(markerMatch.Marker, markerMatch);
                }
            }

            var markerHash = _resultsCollection.TestMarkerDict[testName];

            var markers = new List<Marker>(markerHash);
            markers.Sort((a, b) => System.String.CompareOrdinal(a.Name, b.Name));

            var sb = new StringBuilder();
            foreach (Marker marker in markers)
            {
                if (dict.ContainsKey(marker))
                {
                    sb.Append( dict[marker].CorrectMarkerMatch ? "1":"0");
                }
                else
                {
                    sb.Append("0");
                }
            }
            return string.Format("{0}", sb);
        }

        public Bitmap GetBinaryAbsencePresenceBitmap(string testName)
        {
            var matches = _testMatchDict[testName];

            var dict = new Dictionary<Marker, MarkerMatch>();
            foreach (MarkerMatch markerMatch in matches)
            {
                if (!dict.ContainsKey(markerMatch.Marker))
                {
                    dict.Add(markerMatch.Marker, markerMatch);
                }
            }
            
            var markerHash = _resultsCollection.TestMarkerDict[testName];

            var markers = new List<Marker>(markerHash);
            markers.Sort((a,b) => System.String.CompareOrdinal(a.Name, b.Name));

            var binaryData = new bool[markers.Count];
            int count = 0;
            foreach (Marker marker in markers)
            {
                if (dict.ContainsKey(marker))
                {
                    binaryData[count] = dict[marker].CorrectMarkerMatch;
                }
                else
                {
                    binaryData[count] = false;
                }
                count++;
            }
            return GetBinaryBitmap(binaryData);
        }

        private static Bitmap GetBinaryBitmap(bool[] data)
        {
            var bm = new UnsafeBitmap(new Bitmap(data.Length, 1));
            bm.LockBitmap();
            for (int i = 0; i < data.Length; i++)
            {
                bool next = data[i];
                for (int j = 0; j < 1; j++)
                {
                    var px = new PixelData { Blue = (next) ? (byte)0 : byte.MaxValue, Green = (next) ? (byte)0 : byte.MaxValue, Red = (next) ? (byte)0 : byte.MaxValue };
                    bm.SetPixel(i, j, px);
                }
            }
            bm.UnlockBitmap();
            return bm.Bitmap;
        }

        public ContigCollection ContigCollection { get { return _contigCollection; } }
        public Dictionary<string, List<MarkerMatch>> TestMatchDict { get { return _testMatchDict; } }
        public Dictionary<string, List<string>> TestExtraInfoDict { get { return _testExtraInfoDict; } }
    }
}
