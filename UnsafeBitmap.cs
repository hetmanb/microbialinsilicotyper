using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace MicrobialInSilicoTyper
{
    public unsafe class UnsafeBitmap
    {
        readonly Bitmap _bitmap;

        // three elements used for MakeGreyUnsafe
        int _width;
        BitmapData _bitmapData;
        Byte* _pBase = null;

        public UnsafeBitmap(Bitmap bitmap)
        {
            _bitmap = new Bitmap(bitmap);
        }

        public UnsafeBitmap(int width, int height)
        {
            _bitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb);
        }

        public void Dispose()
        {
            _bitmap.Dispose();
        }

        public Bitmap Bitmap
        {
            get
            {
                return (_bitmap);
            }
        }

        private Point PixelSize
        {
            get
            {
                GraphicsUnit unit = GraphicsUnit.Pixel;
                RectangleF bounds = _bitmap.GetBounds(ref unit);

                return new Point((int)bounds.Width, (int)bounds.Height);
            }
        }

        public void LockBitmap()
        {
            GraphicsUnit unit = GraphicsUnit.Pixel;
            RectangleF boundsF = _bitmap.GetBounds(ref unit);
            var bounds = new Rectangle((int)boundsF.X,
                                       (int)boundsF.Y,
                                       (int)boundsF.Width,
                                       (int)boundsF.Height);

            // Figure out the number of bytes in a row
            // This is rounded up to be a multiple of 4
            // bytes, since a scan line in an image must always be a multiple of 4 bytes
            // in length.
            _width = (int)boundsF.Width * sizeof(PixelData);
            if (_width % 4 != 0)
            {
                _width = 4 * (_width / 4 + 1);
            }
            _bitmapData =
                _bitmap.LockBits(bounds, ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            _pBase = (Byte*)_bitmapData.Scan0.ToPointer();
        }

        public PixelData GetPixel(int x, int y)
        {
            PixelData returnValue = *PixelAt(x, y);
            return returnValue;
        }

        public void SetPixel(int x, int y, PixelData colour)
        {
            PixelData* pixel = PixelAt(x, y);
            *pixel = colour;
        }

        public void UnlockBitmap()
        {
            _bitmap.UnlockBits(_bitmapData);
            _bitmapData = null;
            _pBase = null;
        }
        public PixelData* PixelAt(int x, int y)
        {
            return (PixelData*)(_pBase + y * _width + x * sizeof(PixelData));
        }
    }
}