using System;
using System.Collections.Generic;

namespace MicrobialInSilicoTyper
{
    public static class UPGMA
    {
        private const double EPSILON = double.Epsilon;

        /// <summary>UPGMA cluster binary data stopping at the provided threshold if specified. Create clusters based on threshold.</summary>
        public static int[] Cluster(double[][] distanceMatrix)
        {
            //List<int>[] strains = GetDefaultStrainGroups(d);
            int[][] strains = GetDefaultStrainGroups(distanceMatrix);
            int n = distanceMatrix.Length - 1;
            //by default, run clustering until there is only one cluster
            while (n > 1)
            {
                int minI = 0;
                //x coordinate of where the maximum similarity is found within the distance matrix
                int minJ = 0;
                //y coordinate of where the maximum similarity is found within the distance matrix
                double minD = double.PositiveInfinity;
                //maximum similarity value

                FindMinDifferences(distanceMatrix, n, ref minD, ref minI, ref minJ);

                var ministrains = strains[minI];
                var minjstrains = strains[minJ];
                int strainsMinILength = ministrains.Length;
                int strainsMinJLength = minjstrains.Length;
                var tmpStrains = new int[strainsMinILength + strainsMinJLength];
                if (strainsMinILength > strainsMinJLength)
                {
                    int count = 0;
                    for (int i = 0; i < strainsMinILength; i++)
                    {
                        tmpStrains[count] = ministrains[i];
                        count++;
                    }
                    for (int i = 0; i < strainsMinJLength; i++)
                    {
                        tmpStrains[count] = minjstrains[i];
                        count++;
                    }

                    //                     tmpStrains.AddRange(strains[min_i]);
                    //                     tmpStrains.AddRange(strains[min_j]);
                }
                else
                {
                    int count = 0;
                    for (int i = 0; i < strainsMinJLength; i++)
                    {
                        tmpStrains[count] = minjstrains[i];
                        count++;
                    }
                    for (int i = 0; i < strainsMinILength; i++)
                    {
                        tmpStrains[count] = ministrains[i];
                        count++;
                    }
                    //                     tmpStrains.AddRange(strains[min_j]);
                    //                     tmpStrains.AddRange(strains[min_i]);
                }
                strains[n] = tmpStrains;

                // Compute distance from new tree to every other tree.
                double wI = strainsMinILength / (double)tmpStrains.Length;
                double wJ = strainsMinJLength / (double)tmpStrains.Length;
                double[] dN = distanceMatrix[n];
                for (int k = 0; k < n; k++)
                {
                    double[] dK = distanceMatrix[k];
                    double dNk = wI * dK[minI] + wJ * dK[minJ];
                    dN[k] = dNk;
                    dK[n] = dNk;
                }

                // Swap row <N> with row <min_i> and swap row <N-1> with row
                // <min_j>, thus removing rows <min_i> and <min_j> from D, tree, and
                // n.
                double[] swap1 = distanceMatrix[minI];
                distanceMatrix[minI] = distanceMatrix[n];
                distanceMatrix[n] = swap1;
                swap1 = distanceMatrix[minJ];
                distanceMatrix[minJ] = distanceMatrix[n - 1];
                distanceMatrix[n - 1] = swap1;

                //                 DnaSequenceTree swap2 = tree[min_i];
                //                 tree[min_i] = tree[N];
                //                 tree[N] = swap2;
                //                 swap2 = tree[min_j];
                //                 tree[min_j] = tree[N - 1];
                //                 tree[N - 1] = swap2;

                var swap3 = strains[minI];
                strains[minI] = strains[n];
                strains[n] = swap3;
                swap3 = strains[minJ];
                strains[minJ] = strains[n - 1];
                strains[n - 1] = swap3;

                // Swap column <N> with column <min_i> and swap column <N-1> with
                // column <min_j>, thus removing columns <min_i> and <min_j> from D.
                for (int k = 0; k <= n; ++k)
                {
                    double[] dI = distanceMatrix[k];
                    double swap4 = dI[minI];
                    dI[minI] = dI[n];
                    dI[n] = swap4;
                    swap4 = dI[minJ];
                    dI[minJ] = dI[n - 1];
                    dI[n - 1] = swap4;
                }

                distanceMatrix[n] = null;
                strains[n] = null;
                n--;
            }
            return strains[0];
        }

        private static void FindMinDifferences(double[][] d, int n, ref double minD, ref int minI, ref int minJ)
        {
            //Go through the distance matrix looking for the minimum difference value
            //iterate through the clusters
            for (int i = 0; i < n - 1; i++)
            {
                double[] dmI = d[i];
                //iterate through the values within each cluster
                for (int j = i + 1; j < n; j++)
                {
                    //see if the value is larger than the previous smallest difference value
                    double dtmp = dmI[j];
                    if (dtmp < minD)
                    {
                        minI = i;
                        minJ = j;
                        minD = dtmp;
                        if (Math.Abs(minD - 0) < EPSILON)
                        {
                            return;
                        }
                    }
                }
            }
        }

        public static double[][] GetDistanceMatrix(List<bool[]> binaryData)
        {
            var dm = new double[binaryData.Count + 1][];
            for (int i = 0; i < dm.Length; i++)
            {
                dm[i] = new double[binaryData.Count + 1];
            }

            for (int i = 0; i < binaryData.Count - 1; i++)
            {
                double[] dmI = dm[i];
                for (int j = i + 1; j < binaryData.Count; j++)
                {
                    double dij = GetDifferences(binaryData[i], binaryData[j]);

                    dmI[j] = dij;
                    dm[j][i] = dij;
                }
            }
            return dm;
        }

        public static double GetDifferences(bool[] lb1, bool[] lb2)
        {
            double count = 0;
            for (int i = 0; i < lb1.Length; i++)
            {
                if (lb1[i] != lb2[i])
                    count++;
            }
            return count;
        }

        private static int[][] GetDefaultStrainGroups(double[][] distanceMatrix)
        {
            var rtn = new int[distanceMatrix.Length][];
            for (int i = 0; i < distanceMatrix.Length; i++)
            {
                var tmp = new[] { i };
                rtn[i] = tmp;
            }
            return rtn;
        }
    }
}