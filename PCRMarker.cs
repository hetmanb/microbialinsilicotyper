using System;

namespace MicrobialInSilicoTyper
{
    [Serializable]
    public class PCRMarker : IMarker
    {
        public string Name { get; set; }



        public string TestName { get; set; }



        private TestType _testType;
        public TestType ResultType
        {
            get { return _testType; }
            set { _testType = value; }
        }

        public string ForwardPrimer { get; set; }
        public string ReversePrimer { get; set; }
        public int AmpliconSize { get; set; }
        public double AmpliconRange { get; set; }

        public PCRMarker(
            string testName,
            string name,
            string forwardPrimer,
            string reversePrimer,
            int ampliconSize,
            double ampliconRange) 
        {
            _testType = TestType.PCR;
            TestName = testName;
            Name = name;
            ForwardPrimer = forwardPrimer;
            ReversePrimer = reversePrimer;
            AmpliconSize = ampliconSize;
            AmpliconRange = ampliconRange;
        }

        public Marker GetMarker()
        {
            var p = new Marker(
                name: Name, 
                testName: TestName, 
                testType: TestType.PCR, 
                fprimer: ForwardPrimer, 
                rprimer: ReversePrimer, 
                ampliconSize: AmpliconSize, 
                ampliconRange: AmpliconRange, 
                allelicDatabaseFilename: "", 
                repeatSize: -1
                );
            return p;
        }


    }
}