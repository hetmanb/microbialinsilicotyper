﻿using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using BrightIdeasSoftware;

namespace MicrobialInSilicoTyper
{
    public partial class CreateAlleleTestForm : Form
    {
        private readonly PackageTest _test;

        readonly List<AlleleMarker> _markers = new List<AlleleMarker>();

        private string _testName;

        public CreateAlleleTestForm(PackageTest test)
        {
            _test = test;
            InitializeComponent();

            txtExtraInfo.TextChanged += (sender, args) => txtExtraInfo.Select(txtExtraInfo.Text.Length, 0);
            btnOK.DialogResult = DialogResult.OK;
            btnCancel.DialogResult = DialogResult.Cancel;

            SetupOLV();

            //check if new test
            if (_test == null || _test.Markers.Count == 0)
            {
                _testName = txtTestName.Text;
            }
            else
            {//setup existing test - user is changing something
                InitTestMarkers();
                txtTestName.Text = _test.TestName;
                _testName = _test.TestName;
                txtExtraInfo.Text=Misc.GetExtraTestInfoFilePath(_testName);
            }

            txtTestName.KeyPress += (sender, args) => OnTestNameKeyPress(args);
            txtTestName.LostFocus += (sender, args) => OnTestNameKeyPress(new KeyPressEventArgs((char)Keys.Enter));
            txtTestName.TextChanged += (sender, args) => btnOK.Enabled = txtTestName.Text != "" && _markers.Count > 0;

            txtExtraInfo.AllowDrop = true;
            txtExtraInfo.DragEnter += (sender, args) => Misc.OnDragOverOrEnter(args);
            txtExtraInfo.DragOver += (sender, args) => Misc.OnDragOverOrEnter(args);
            //don't allow the user to change the text within txtExtraInfo by keyboard presses
            txtExtraInfo.KeyPress += (sender, args) => args.Handled = true;
            txtExtraInfo.DragDrop += (sender, args) =>
            {
                if (!args.Data.GetDataPresent(DataFormats.FileDrop)) 
                    return;
                var files = (string[])args.Data.GetData(DataFormats.FileDrop);
                if (files.Length != 1) 
                    return;
                var filename = files[0];
                txtExtraInfo.Text = filename;
            };
            btnExtraInfo.Click += (sender, args) =>
            {
                var sfd = new OpenFileDialog
                              {
                                  Multiselect = false, 
                                  Title = "Select extra information file"
                              };
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    txtExtraInfo.Text = sfd.FileName;
                }
            };

            btnOK.Enabled = txtTestName.Text != "" && _markers.Count > 0;
            btnOK.Click += (sender, args) => OnOKClick();
        }



        private void OnOKClick()
        {
            _test.Clear();
            foreach (IMarker bm in _markers)
            {
                _test.Add(bm.GetMarker());
            }
            _test.WritePrimerFile(Path.Combine(_test.PackageDir.FullName, string.Format("{0}.markers", _test.TestName)));
            if (txtExtraInfo.Text == "")
                return;
            var extraInfoFile = new FileInfo(txtExtraInfo.Text);
            if (!extraInfoFile.Exists)
                return;
            string newFilePath = Path.Combine(_test.PackageDir.FullName, string.Format("{0}.txt", txtTestName.Text));
            if (newFilePath == extraInfoFile.FullName)
                return;
            extraInfoFile.CopyTo(newFilePath, true);

        }

        private void OnTestNameKeyPress(KeyPressEventArgs args)
        {
            if (args.KeyChar == (char)Keys.Enter || args.KeyChar == '\t')
            {
                _testName = txtTestName.Text;

                foreach (var marker in _markers)
                {
                    marker.TestName = _testName;
                }
                olv.Refresh();
            }
            else
            {
                args.Handled = false;
            }
        }

        private void SetupOLV()
        {
            olv.CellEditActivation = ObjectListView.CellEditActivateMode.DoubleClick;
            olv.FullRowSelect = true;
            olv.UseTranslucentSelection = true;
            olv.OwnerDraw = true;
            olv.UseAlternatingBackColors = true;
            olv.AllowDrop = true;

            olv.Columns.Add(new OLVColumn("Name", "Name") { FillsFreeSpace = true });
            olv.Columns.Add(new OLVColumn("Test Name", "TestName") { IsEditable = false, FillsFreeSpace = true });
            olv.Columns.Add(new OLVColumn("Allele File Path", "AlleleFilePath") { IsEditable = false});

            olv.DragEnter += (s, e) => Misc.OnDragOverOrEnter(e);
            olv.DragOver += (s, e) => Misc.OnDragOverOrEnter(e);
            olv.DragDrop += (s, e) => OnFileDrop(e);

            olv.SetObjects(_markers);
        }

        private void OnFileDrop(DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(DataFormats.FileDrop))
                return;
            var filenames = (string[])e.Data.GetData(DataFormats.FileDrop);
            foreach (string filename in filenames)
            {
                var fileinfo = new FileInfo(filename);
                //check if file is multifasta format
                if (!Misc.ValidateFileDrop(fileinfo))
                    continue;
                //get the name of the allele from the filename
                string name = fileinfo.Name.Remove(fileinfo.Name.LastIndexOf('.'));
                _markers.Add(new AlleleMarker(_testName, name, fileinfo.FullName, _test.PackageDir));
            }
            olv.SetObjects(_markers);
        }

        private void InitTestMarkers()
        {
            foreach (Marker m in _test.Markers)
            {
                if (m.TypingTest == TestType.Allelic)
                {
                    _markers.Add(new AlleleMarker(
                                     m.TestName,
                                     m.Name,
                                     m.AllelicDatabaseFilename,
                                     _test.PackageDir));
                }
            }
            olv.SetObjects(_markers);
        }
    }
}
