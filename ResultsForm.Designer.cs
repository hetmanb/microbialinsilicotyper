﻿

namespace MicrobialInSilicoTyper
{
    partial class ResultsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResultsForm));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnSaveVerbose = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSaveSparse = new System.Windows.Forms.ToolStripMenuItem();
            this.btnShowMatchInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ddbFile = new System.Windows.Forms.ToolStripDropDownButton();
            this.saveToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSaveVerbose2 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSaveSparse2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ddbEdit = new System.Windows.Forms.ToolStripDropDownButton();
            this.chkFuzzyMatches = new System.Windows.Forms.ToolStripMenuItem();
            this.btnResetResults = new System.Windows.Forms.ToolStripMenuItem();
            this.ddbAnalyze = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnCalcPartitionCongruence = new System.Windows.Forms.ToolStripMenuItem();
            this.btnShowMatchInfo2 = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSaveVerbose,
            this.btnSaveSparse,
            this.btnShowMatchInfo});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(314, 70);
            // 
            // btnSaveVerbose
            // 
            this.btnSaveVerbose.Name = "btnSaveVerbose";
            this.btnSaveVerbose.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.btnSaveVerbose.Size = new System.Drawing.Size(313, 22);
            this.btnSaveVerbose.Text = "Save Typing Results (Verbose)";
            // 
            // btnSaveSparse
            // 
            this.btnSaveSparse.Name = "btnSaveSparse";
            this.btnSaveSparse.Size = new System.Drawing.Size(313, 22);
            this.btnSaveSparse.Text = "Save Typing Results (Sparse)";
            // 
            // btnShowMatchInfo
            // 
            this.btnShowMatchInfo.Name = "btnShowMatchInfo";
            this.btnShowMatchInfo.Size = new System.Drawing.Size(313, 22);
            this.btnShowMatchInfo.Text = "Show Underlying Data For Selected Sample(s)";
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(587, 387);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(587, 412);
            this.toolStripContainer1.TabIndex = 1;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ddbFile,
            this.ddbEdit,
            this.ddbAnalyze});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(151, 25);
            this.toolStrip1.TabIndex = 0;
            // 
            // ddbFile
            // 
            this.ddbFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem1});
            this.ddbFile.Image = ((System.Drawing.Image)(resources.GetObject("ddbFile.Image")));
            this.ddbFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbFile.Name = "ddbFile";
            this.ddbFile.Size = new System.Drawing.Size(38, 22);
            this.ddbFile.Text = "File";
            // 
            // saveToolStripMenuItem1
            // 
            this.saveToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSaveVerbose2,
            this.btnSaveSparse2});
            this.saveToolStripMenuItem1.Name = "saveToolStripMenuItem1";
            this.saveToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.saveToolStripMenuItem1.Text = "Save";
            // 
            // btnSaveVerbose2
            // 
            this.btnSaveVerbose2.Name = "btnSaveVerbose2";
            this.btnSaveVerbose2.Size = new System.Drawing.Size(204, 22);
            this.btnSaveVerbose2.Text = "Typing Results (Verbose)";
            // 
            // btnSaveSparse2
            // 
            this.btnSaveSparse2.Name = "btnSaveSparse2";
            this.btnSaveSparse2.Size = new System.Drawing.Size(204, 22);
            this.btnSaveSparse2.Text = "Typing Results (Sparse)";
            // 
            // ddbEdit
            // 
            this.ddbEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chkFuzzyMatches,
            this.btnResetResults,
            this.btnShowMatchInfo2});
            this.ddbEdit.Image = ((System.Drawing.Image)(resources.GetObject("ddbEdit.Image")));
            this.ddbEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbEdit.Name = "ddbEdit";
            this.ddbEdit.Size = new System.Drawing.Size(40, 22);
            this.ddbEdit.Text = "Edit";
            // 
            // chkFuzzyMatches
            // 
            this.chkFuzzyMatches.CheckOnClick = true;
            this.chkFuzzyMatches.Name = "chkFuzzyMatches";
            this.chkFuzzyMatches.Size = new System.Drawing.Size(230, 22);
            this.chkFuzzyMatches.Text = "Show partial matches";
            // 
            // btnResetResults
            // 
            this.btnResetResults.Name = "btnResetResults";
            this.btnResetResults.Size = new System.Drawing.Size(230, 22);
            this.btnResetResults.Text = "Revert Results Back to Default";
            // 
            // ddbAnalyze
            // 
            this.ddbAnalyze.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbAnalyze.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnCalcPartitionCongruence});
            this.ddbAnalyze.Image = ((System.Drawing.Image)(resources.GetObject("ddbAnalyze.Image")));
            this.ddbAnalyze.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbAnalyze.Name = "ddbAnalyze";
            this.ddbAnalyze.Size = new System.Drawing.Size(61, 22);
            this.ddbAnalyze.Text = "Analyze";
            // 
            // btnCalcPartitionCongruence
            // 
            this.btnCalcPartitionCongruence.Name = "btnCalcPartitionCongruence";
            this.btnCalcPartitionCongruence.Size = new System.Drawing.Size(316, 22);
            this.btnCalcPartitionCongruence.Text = "Compare Typing Method Cluster Congruence";
            // 
            // btnShowMatchInfo2
            // 
            this.btnShowMatchInfo2.Name = "btnShowMatchInfo2";
            this.btnShowMatchInfo2.Size = new System.Drawing.Size(230, 22);
            this.btnShowMatchInfo2.Text = "Show Underlying Data";
            // 
            // ResultsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(587, 412);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "ResultsForm";
            this.Text = "In Silico Typing Results";
            this.contextMenuStrip1.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem btnSaveVerbose;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton ddbFile;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem btnSaveSparse;
        private System.Windows.Forms.ToolStripMenuItem btnShowMatchInfo;
        private System.Windows.Forms.ToolStripMenuItem btnSaveVerbose2;
        private System.Windows.Forms.ToolStripMenuItem btnSaveSparse2;
        private System.Windows.Forms.ToolStripDropDownButton ddbEdit;
        private System.Windows.Forms.ToolStripMenuItem chkFuzzyMatches;
        private System.Windows.Forms.ToolStripMenuItem btnResetResults;
        private System.Windows.Forms.ToolStripDropDownButton ddbAnalyze;
        private System.Windows.Forms.ToolStripMenuItem btnCalcPartitionCongruence;
        private System.Windows.Forms.ToolStripMenuItem btnShowMatchInfo2;
    }
}