﻿namespace MicrobialInSilicoTyper
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.pbr = new System.Windows.Forms.ToolStripProgressBar();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.gbxPackages = new System.Windows.Forms.GroupBox();
            this.btnRemovePackage = new System.Windows.Forms.Button();
            this.btnEditPackage = new System.Windows.Forms.Button();
            this.btnNewPackage = new System.Windows.Forms.Button();
            this.olvPackages = new BrightIdeasSoftware.FastObjectListView();
            this.olvTests = new BrightIdeasSoftware.FastObjectListView();
            this.olvFasta = new BrightIdeasSoftware.FastObjectListView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ddbFile = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAddGenomes = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnClose = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnExit = new System.Windows.Forms.ToolStripMenuItem();
            this.ddbEdit = new System.Windows.Forms.ToolStripDropDownButton();
            this.bLASTWordSizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtBlastWordSize = new System.Windows.Forms.ToolStripTextBox();
            this.numberOfCoresThreadsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtCores = new System.Windows.Forms.ToolStripTextBox();
            this.chkAutosave = new System.Windows.Forms.ToolStripMenuItem();
            this.ddbView = new System.Windows.Forms.ToolStripDropDownButton();
            this.resultsFormToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRun = new System.Windows.Forms.ToolStripButton();
            this.btnCancel = new System.Windows.Forms.ToolStripButton();
            this.bgw = new System.ComponentModel.BackgroundWorker();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.gbxPackages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olvPackages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.olvTests)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.olvFasta)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(701, 437);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(701, 484);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.pbr});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(701, 22);
            this.statusStrip1.TabIndex = 0;
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(45, 17);
            this.lblStatus.Text = "Ready";
            // 
            // pbr
            // 
            this.pbr.Name = "pbr";
            this.pbr.Size = new System.Drawing.Size(100, 16);
            this.pbr.Visible = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.olvFasta);
            this.splitContainer1.Size = new System.Drawing.Size(701, 437);
            this.splitContainer1.SplitterDistance = 233;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.gbxPackages);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.olvTests);
            this.splitContainer2.Size = new System.Drawing.Size(233, 437);
            this.splitContainer2.SplitterDistance = 209;
            this.splitContainer2.TabIndex = 0;
            // 
            // gbxPackages
            // 
            this.gbxPackages.Controls.Add(this.btnRemovePackage);
            this.gbxPackages.Controls.Add(this.btnEditPackage);
            this.gbxPackages.Controls.Add(this.btnNewPackage);
            this.gbxPackages.Controls.Add(this.olvPackages);
            this.gbxPackages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxPackages.Location = new System.Drawing.Point(0, 0);
            this.gbxPackages.Name = "gbxPackages";
            this.gbxPackages.Size = new System.Drawing.Size(229, 205);
            this.gbxPackages.TabIndex = 0;
            this.gbxPackages.TabStop = false;
            this.gbxPackages.Text = "Packages";
            // 
            // btnRemovePackage
            // 
            this.btnRemovePackage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRemovePackage.Location = new System.Drawing.Point(10, 176);
            this.btnRemovePackage.Name = "btnRemovePackage";
            this.btnRemovePackage.Size = new System.Drawing.Size(59, 23);
            this.btnRemovePackage.TabIndex = 2;
            this.btnRemovePackage.Text = "Remove";
            this.btnRemovePackage.UseVisualStyleBackColor = true;
            // 
            // btnEditPackage
            // 
            this.btnEditPackage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditPackage.Location = new System.Drawing.Point(137, 176);
            this.btnEditPackage.Name = "btnEditPackage";
            this.btnEditPackage.Size = new System.Drawing.Size(38, 23);
            this.btnEditPackage.TabIndex = 1;
            this.btnEditPackage.Text = "Edit";
            this.btnEditPackage.UseVisualStyleBackColor = true;
            // 
            // btnNewPackage
            // 
            this.btnNewPackage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNewPackage.Location = new System.Drawing.Point(181, 176);
            this.btnNewPackage.Name = "btnNewPackage";
            this.btnNewPackage.Size = new System.Drawing.Size(42, 23);
            this.btnNewPackage.TabIndex = 0;
            this.btnNewPackage.Text = "New";
            this.btnNewPackage.UseVisualStyleBackColor = true;
            // 
            // olvPackages
            // 
            this.olvPackages.CheckBoxes = false;
            this.olvPackages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvPackages.Location = new System.Drawing.Point(3, 16);
            this.olvPackages.Name = "olvPackages";
            this.olvPackages.ShowGroups = false;
            this.olvPackages.Size = new System.Drawing.Size(223, 186);
            this.olvPackages.TabIndex = 0;
            this.olvPackages.UseCompatibleStateImageBehavior = false;
            this.olvPackages.View = System.Windows.Forms.View.Details;
            this.olvPackages.VirtualMode = true;
            // 
            // olvTests
            // 
            this.olvTests.CheckBoxes = false;
            this.olvTests.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvTests.Location = new System.Drawing.Point(0, 0);
            this.olvTests.Name = "olvTests";
            this.olvTests.ShowGroups = false;
            this.olvTests.Size = new System.Drawing.Size(229, 220);
            this.olvTests.TabIndex = 0;
            this.olvTests.UseCompatibleStateImageBehavior = false;
            this.olvTests.View = System.Windows.Forms.View.Details;
            this.olvTests.VirtualMode = true;
            // 
            // olvFasta
            // 
            this.olvFasta.CheckBoxes = false;
            this.olvFasta.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvFasta.Location = new System.Drawing.Point(0, 0);
            this.olvFasta.Name = "olvFasta";
            this.olvFasta.ShowGroups = false;
            this.olvFasta.Size = new System.Drawing.Size(460, 433);
            this.olvFasta.TabIndex = 0;
            this.olvFasta.UseCompatibleStateImageBehavior = false;
            this.olvFasta.View = System.Windows.Forms.View.Details;
            this.olvFasta.VirtualMode = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ddbFile,
            this.ddbEdit,
            this.ddbView,
            this.btnRun,
            this.btnCancel});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(198, 25);
            this.toolStrip1.TabIndex = 0;
            // 
            // ddbFile
            // 
            this.ddbFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.btnAddGenomes,
            this.toolStripSeparator2,
            this.btnClose,
            this.toolStripSeparator3,
            this.btnExit});
            this.ddbFile.Image = ((System.Drawing.Image)(resources.GetObject("ddbFile.Image")));
            this.ddbFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbFile.Name = "ddbFile";
            this.ddbFile.Size = new System.Drawing.Size(42, 22);
            this.ddbFile.Text = "File";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(156, 6);
            // 
            // btnAddGenomes
            // 
            this.btnAddGenomes.Name = "btnAddGenomes";
            this.btnAddGenomes.Size = new System.Drawing.Size(159, 22);
            this.btnAddGenomes.Text = "Add Genomes";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(156, 6);
            // 
            // btnClose
            // 
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(159, 22);
            this.btnClose.Text = "Close";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(156, 6);
            // 
            // btnExit
            // 
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(159, 22);
            this.btnExit.Text = "Exit";
            // 
            // ddbEdit
            // 
            this.ddbEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bLASTWordSizeToolStripMenuItem,
            this.txtBlastWordSize,
            this.numberOfCoresThreadsToolStripMenuItem,
            this.txtCores,
            this.chkAutosave});
            this.ddbEdit.Image = ((System.Drawing.Image)(resources.GetObject("ddbEdit.Image")));
            this.ddbEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbEdit.Name = "ddbEdit";
            this.ddbEdit.Size = new System.Drawing.Size(44, 22);
            this.ddbEdit.Text = "Edit";
            // 
            // bLASTWordSizeToolStripMenuItem
            // 
            this.bLASTWordSizeToolStripMenuItem.Name = "bLASTWordSizeToolStripMenuItem";
            this.bLASTWordSizeToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.bLASTWordSizeToolStripMenuItem.Text = "BLAST Word Size (PCR)";
            // 
            // txtBlastWordSize
            // 
            this.txtBlastWordSize.Name = "txtBlastWordSize";
            this.txtBlastWordSize.Size = new System.Drawing.Size(100, 22);
            this.txtBlastWordSize.Text = "7";
            // 
            // numberOfCoresThreadsToolStripMenuItem
            // 
            this.numberOfCoresThreadsToolStripMenuItem.Name = "numberOfCoresThreadsToolStripMenuItem";
            this.numberOfCoresThreadsToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.numberOfCoresThreadsToolStripMenuItem.Text = "Number of Cores/Threads:";
            // 
            // txtCores
            // 
            this.txtCores.Name = "txtCores";
            this.txtCores.Size = new System.Drawing.Size(215, 22);
            this.txtCores.Text = "1";
            // 
            // chkAutosave
            // 
            this.chkAutosave.Name = "chkAutosave";
            this.chkAutosave.Size = new System.Drawing.Size(294, 22);
            this.chkAutosave.Text = "Autosave analysis when run complete";
            // 
            // ddbView
            // 
            this.ddbView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resultsFormToolStripMenuItem});
            this.ddbView.Image = ((System.Drawing.Image)(resources.GetObject("ddbView.Image")));
            this.ddbView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbView.Name = "ddbView";
            this.ddbView.Size = new System.Drawing.Size(49, 22);
            this.ddbView.Text = "View";
            // 
            // resultsFormToolStripMenuItem
            // 
            this.resultsFormToolStripMenuItem.Name = "resultsFormToolStripMenuItem";
            this.resultsFormToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.resultsFormToolStripMenuItem.Text = "Typing Results Summary";
            // 
            // btnRun
            // 
            this.btnRun.Enabled = false;
            this.btnRun.Image = ((System.Drawing.Image)(resources.GetObject("btnRun.Image")));
            this.btnRun.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(51, 22);
            this.btnRun.Text = "Run";
            // 
            // btnCancel
            // 
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(68, 22);
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Visible = false;
            // 
            // bgw
            // 
            this.bgw.WorkerReportsProgress = true;
            this.bgw.WorkerSupportsCancellation = true;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "marker";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Test Name";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Test Type";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Forward Primer";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Reverse Primer";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Amplicon Size";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Amplicon Size Range";
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Allelic Database Filename";
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Repeat Size";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 484);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "MainForm";
            this.Text = "MicrobialInSilicoTyper - MIST";
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.gbxPackages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.olvPackages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.olvTests)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.olvFasta)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton ddbFile;
        private System.Windows.Forms.ToolStripMenuItem btnClose;
        private System.Windows.Forms.ToolStripMenuItem btnExit;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStripDropDownButton ddbView;
        private System.Windows.Forms.ToolStripButton btnRun;
        private System.ComponentModel.BackgroundWorker bgw;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ToolStripProgressBar pbr;
        private System.Windows.Forms.ToolStripMenuItem resultsFormToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton btnCancel;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.GroupBox gbxPackages;
        private BrightIdeasSoftware.FastObjectListView olvPackages;
        private System.Windows.Forms.Button btnRemovePackage;
        private System.Windows.Forms.Button btnEditPackage;
        private System.Windows.Forms.Button btnNewPackage;
        private BrightIdeasSoftware.FastObjectListView olvTests;
        private BrightIdeasSoftware.FastObjectListView olvFasta;
        private System.Windows.Forms.ToolStripDropDownButton ddbEdit;
        private System.Windows.Forms.ToolStripMenuItem bLASTWordSizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtBlastWordSize;
        private System.Windows.Forms.ToolStripMenuItem numberOfCoresThreadsToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtCores;
        private System.Windows.Forms.ToolStripMenuItem chkAutosave;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ToolStripMenuItem btnAddGenomes;
    }
}

