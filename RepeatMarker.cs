﻿using System;

namespace MicrobialInSilicoTyper
{
    [Serializable]
    public class RepeatMarker : IMarker
    {
        public string TestName { get; set; }

        public string Name { get; set; }

        private TestType _testType;
        public TestType ResultType { get { return _testType; }set { _testType = value; } }

        public string ForwardPrimer { get; set; }
        public string ReversePrimer { get; set; }
        public int OffsetSize { get; set; }
        public int RepeatLength { get; set; }

        public RepeatMarker(
            string testName,
            string name,
            string forwardPrimer,
            string reversePrimer,
            int offsetSize,
            int repeatLength)
        {
            _testType = TestType.Repeat;
            TestName = testName;
            Name = name;
            ForwardPrimer = forwardPrimer;
            ReversePrimer = reversePrimer;
            OffsetSize = offsetSize;
            RepeatLength = repeatLength;
        }

        public Marker GetMarker()
        {
            var p = new Marker(
                name: Name,
                testName: TestName,
                testType: _testType,
                fprimer: ForwardPrimer,
                rprimer: ReversePrimer,
                ampliconSize: OffsetSize,
                ampliconRange: -1,
                allelicDatabaseFilename: "",
                repeatSize: RepeatLength
                );
            return p;
        }
    }
}
