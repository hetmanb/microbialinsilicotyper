﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace MicrobialInSilicoTyper
{
    public class PackageTest
    {
        private readonly List<Marker> _markers = new List<Marker>();

        private readonly DirectoryInfo _packageDir;

        public PackageTest(DirectoryInfo packageDir)
        {
            _packageDir = packageDir;
        }

        public PackageTest(DirectoryInfo packageDir, FileInfo filename)
        {
            _packageDir = packageDir;
            ReadPrimerFile(filename);
        }

        public List<Marker> Markers
        {
            get { return _markers; }
        }

        public DirectoryInfo PackageDir
        {
            get { return _packageDir; }
        }

        public string TestName
        {
            get { return _markers[0].TestName; }
        }

        public TestType TestType
        {
            get { return _markers[0].TypingTest; }
        }

        public void Add(Marker marker)
        {
            _markers.Add(marker);
        }

        public void Clear()
        {
            _markers.Clear();
        }

        private void ReadPrimerFile(FileInfo filename)
        {
            using (var sr = new StreamReader(filename.FullName))
            {
                bool noError = true;
                if (filename.DirectoryName == null)
                {
                }
                else
                {
                    var dir = new DirectoryInfo(filename.DirectoryName);
                    var lPrimers = new List<Marker>();
                    try
                    {
                        // All test types                                                           || binary/allelic|| binary       || allelic only              || repeat only
                        //marker Name || Test Name || Test Type || Forward Primer || Reverse Primer || Amplicon Size || Range Factor || Allelic Database Filename || Repeat Size 
                        // 0          ||     1     ||     2     ||      3         ||        4       ||       5       ||     6        ||             7             ||    8
                        sr.ReadLine(); //skip first line; headers
                        while (!sr.EndOfStream)
                        {
                            string readLine = sr.ReadLine();
                            if (readLine == null) continue;
                            string[] tmp = readLine.Split('\t');
                            int i, j;
                            double d;
                            lPrimers.Add(new Marker(
                                             tmp[0],
                                             tmp[1],
                                             (TestType) (int.Parse(tmp[2])),
                                             tmp[3],
                                             tmp[4],
                                             int.TryParse(tmp[5], out i) ? i : -1,
                                             double.TryParse(tmp[6], NumberStyles.Any, CultureInfo.InvariantCulture, out d) ? d : -1,
                                             tmp[7] == ""
                                                 ? ""
                                                 : dir.GetFiles(tmp[7], SearchOption.AllDirectories)[0].FullName,
                                             int.TryParse(tmp[8], out j) ? j : -1
                                             ));
                        }
                    }
                    catch (Exception)
                    {
                        noError = false;
                    }
                    finally
                    {
                        if (noError)
                            _markers.AddRange(lPrimers);
                    }
                }
            }
        }

        public void WritePrimerFile(string filename)
        {
            using (var sw = new StreamWriter(filename))
            {
                //marker Name	Test Name	Test Type	Forward Primer	Reverse Primer	Amplicon Size (bp)	Amplicon Range Factor (e.g. 0.1)	Allelic Database Filename	Repeat Size (bp)
                sw.WriteLine(
                    "marker Name\tTest Name\tTest Type\tForward Primer\tReverse Primer\tAmplicon Size (bp)\tAmplicon Range Factor (e.g. 0.1)\tAllelic Database Filename\tRepeat Size (bp)");
                foreach (Marker p in _markers)
                {
                    sw.WriteLine(p.ToString());
                }
            }
        }
    }
}