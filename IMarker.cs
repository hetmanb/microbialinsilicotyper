namespace MicrobialInSilicoTyper
{
    public  interface IMarker
    {
        string TestName { get; set; }
        string Name { get; set; }
        TestType ResultType { get; set; }

        Marker GetMarker();
    }
}