﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace MicrobialInSilicoTyper
{
    public enum Progress
    {
        Incomplete,
        Complete,
        InProgress
    }

    public static class Misc
    {
        /// <summary>Limit for the size of a PCR amplicon is 3kbp. Unlikely that a PCR product larger than this will be generated.</summary>
        public const int MaxAmpliconSizeAllowable = 3000;
        public static int BlastErrorRetryLimit { get; set; }
        public static readonly Regex NumberRegex = new Regex(@"\d+", RegexOptions.Compiled);
        private static readonly object ThisLock = new object();

        public static DirectoryInfo TempDir { get; set; }

        public static int MinWorkerThreads;
        public static int MaxWorkerThreads;

        public static int MinCompletedThreads;
        public static int MaxCompletedThreads;

        private static int _cores;

        public static int Cores
        {
            get { return _cores; }
            set
            {
                _cores = value;
                ThreadPool.SetMinThreads(_cores, MinCompletedThreads);
                ThreadPool.SetMaxThreads(_cores, MaxCompletedThreads);
            }
        }

        /// <summary>BLAST word size (default: 11)</summary>
        public static int BlastWordSize { get; set; }

        public static void GetDefaultThreadPoolWorkerThreads()
        {
            ThreadPool.GetMinThreads(out MinWorkerThreads, out MinCompletedThreads);
            ThreadPool.GetMaxThreads(out MaxWorkerThreads, out MaxCompletedThreads);
        }

        public static void SetDefaultThreadPoolWorkerThreads()
        {
            ThreadPool.SetMinThreads(MinWorkerThreads, MinCompletedThreads);
            ThreadPool.SetMaxThreads(MaxWorkerThreads, MaxCompletedThreads);
        }

        public static bool IsDegenSequence(string sequence)
        {
            foreach (char c in sequence)
            {
                switch (c)
                {
                    case 'A':
                    case 'C':
                    case 'G':
                    case 'T':
                        break;
                    default:
                        return true;
                }
            }
            return false;
        }

        /// <summary>Expand a degenerate nucleotide sequence into all of its possible non-degenerate sequences.
        /// Returns list of non-degenerate sequences.</summary>
        /// <param name="array">Mutable char array for holding nucleotide sequence for current non-degenerate sequence.</param>
        /// <param name="seq">Degenerate nucleotide sequence.</param>
        /// <param name="index">Current index in the degenerate nucleotide sequence.</param>
        /// <param name="list">List of non-degenerate sequences to be returned.</param>
        public static void ExpandDegenSequence(char[] array, string seq, int index, List<string> list)
        {
            if (index == seq.Length)
            {
                list.Add(new string(array));
                return;
            }

            char c = seq[index];
            var charList = new List<char>();
            switch (c)
            {
                case 'A':
                    charList.Add('A');
                    break;
                case 'T':
                    charList.Add('T');
                    break;
                case 'G':
                    charList.Add('G');
                    break;
                case 'C':
                    charList.Add('C');
                    break;
                case 'R':
                    //A or G
                    charList.Add('A');
                    charList.Add('G');
                    break;
                case 'Y':
                    //C or T
                    charList.Add('C');
                    charList.Add('T');
                    break;
                case 'M':
                    //A or C
                    charList.Add('A');
                    charList.Add('C');
                    break;
                case 'S':
                    //G or C
                    charList.Add('G');
                    charList.Add('C');
                    break;
                case 'W':
                    //A or T
                    charList.Add('A');
                    charList.Add('T');
                    break;
                case 'K':
                    //G or T
                    charList.Add('G');
                    charList.Add('T');
                    break;
                case 'V':
                    //A, C or G; not T
                    charList.Add('A');
                    charList.Add('C');
                    charList.Add('G');
                    break;
                case 'D':
                    //A, G or T; not C
                    charList.Add('A');
                    charList.Add('G');
                    charList.Add('T');
                    break;
                case 'H':
                    //A, C or T; not G
                    charList.Add('A');
                    charList.Add('C');
                    charList.Add('T');
                    break;
                case 'B':
                    //C, G or T; not A
                    charList.Add('C');
                    charList.Add('G');
                    charList.Add('T');
                    break;
                case 'N':
                    //aNy
                    charList.Add('A');
                    charList.Add('C');
                    charList.Add('G');
                    charList.Add('T');
                    break;
            }
            foreach (char c1 in charList)
            {
                array[index] = c1;
                ExpandDegenSequence(array, seq, index + 1, list);
            }
        }

        /// <summary>Get the number of similar characters between two strings. Strings must be of the same length.</summary>
        /// <param name="s1">String</param>
        /// <param name="s2">String</param>
        /// <returns>Count of similiarities between two strings.</returns>
        public static int GetDifferences(string s1, string s2)
        {
            int count = 0;
            //only search the strings for the length of the shorter string
            int minLength = s1.Length > s2.Length ? s2.Length : s1.Length;
            for (int i = 0; i < minLength; i++)
            {
                if (s1[i] != s2[i])
                    count++;
            }
            return count;
        }

        public static void OnDragOverOrEnter(DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop) || e.Data.GetDataPresent(DataFormats.UnicodeText))
                e.Effect = DragDropEffects.Copy;
        }

        public static bool ValidateFileDrop(FileInfo fileInfo)
        {
            //only check that the first line contains a '>'
            using (var sr = new StreamReader(fileInfo.FullName))
            {
                string line = sr.ReadLine();
                return !String.IsNullOrEmpty(line) && line.Contains(">");
            }
        }

        /// <summary>Is the app using the Mono runtime, i.e. running under Linux?</summary>
        public static bool UsingMonoRuntime()
        {
            return Type.GetType("Mono.Runtime") != null;
        }

        public static FileInfo WriteTempMultifastaFile(FileInfo multifasta, DirectoryInfo tmpDir, out int fastaEntries)
        {
            var contigCollection = new ContigCollection(multifasta.FullName);
            contigCollection.Read();
            fastaEntries = contigCollection.Contigs.Count;
            string path = Path.Combine(tmpDir.FullName, multifasta.Name.Replace(' ', '_'));
            using (var sw = new StreamWriter(path))
            {
                foreach (Contig contig in contigCollection.Contigs)
                {
                    if (contig.Sequence.Length > 0)
                    {
                        sw.Write(">");
                        sw.WriteLine(contig.Index);
                        sw.WriteLine(contig.Sequence);
                    }
                }
            }
            return new FileInfo(path);
        }

        public static bool GetAmplicon(int startIndex, int endIndex, bool reverseComplement, Contig c, ref string amplicon)
        {
            lock (ThisLock)
            {
                try
                {
                    var isContigTruncation = false;
                    //get the amplicon sequence with the adjusted start and end indices
                    if (startIndex < 0)
                    {
                        //if the amplicon extends to an end of a contig
                        isContigTruncation = true;
                        amplicon = c.Sequence.Length < endIndex ?
                            c.Sequence :
                            c.Sequence.Substring(0, endIndex + 1);

                    }
                    else
                    {
                        //if the adjusted amplicon length is longer than the actual length of the sequence 
                        //then go for the longest possible bit of sequence
                        if ((c.Sequence.Length - startIndex) < (endIndex - startIndex + 1))
                        {
                            isContigTruncation = true;
                            //Console.Error.WriteLine(string.Format("Amplicon contig truncation detected with contig {0} of multifasta {1}", c.Header, c.MultifastaFile.Name))
                            amplicon = c.Sequence.Substring(startIndex, (c.Sequence.Length - startIndex));
                        }
                        else
                        {
                            amplicon = c.Sequence.Substring(startIndex, endIndex - startIndex + 1);
                        }
                    }

                    amplicon = reverseComplement ? ReverseComplement(amplicon) : amplicon.ToUpper();
                    if (isContigTruncation)
                    {
                        Console.Error.WriteLine(string.Format("Amplicon contig truncation detected with contig {0} of multifasta {1}", c.Header, c.MultifastaFile.Name));
                    }
                    return isContigTruncation;
                }
                catch (Exception)
                {
                    return true;
                }
            }

        }

        public static void AdjustSubjectIndices(int length, ref int queryEndIndex, ref int queryStartIndex, bool reverseComplement, ref int subjectEndIndex, ref int subjectStartIndex)
        {
            int addToEnd = length - queryEndIndex;
            int addToStart = queryStartIndex - 1;

            if (reverseComplement)
            {
                subjectEndIndex--; //-1 to match array indices which start at 0
                subjectStartIndex--; //-1 to match array indices which start at 0
                subjectEndIndex += addToStart;
                subjectStartIndex -= addToEnd;
            }
            else
            {
                subjectStartIndex -= addToStart;
                subjectStartIndex--; //-1 to match array indices which start at 0
                subjectEndIndex += addToEnd;
                subjectEndIndex--; //-1 to match array indices which start at 0
            }
        }


        /// <summary>Run makeblastdb on multifasta file.</summary>
        /// <param name="workingDir">Working directory; "Temp" folder within folder where the genome multifasta files were selected.</param>
        /// <param name="fiMultifasta">Genome multifasta file.</param>
        public static void MakeBlastDB(DirectoryInfo workingDir, FileInfo fiMultifasta)
        {
            var startInfo = new ProcessStartInfo(@"makeblastdb.exe",
                                                 String.Format(@"-in ""{0}"" -dbtype nucl",
                                                               fiMultifasta.Name.Replace(' ', '_')))
                                {
                                    WorkingDirectory = workingDir.FullName,
                                    RedirectStandardError = true,
                                    RedirectStandardOutput = true,
                                    UseShellExecute = false,
                                    CreateNoWindow = true
                                };
            var p = new Process {StartInfo = startInfo};
            while (!p.Start())
            {
                p = new Process {StartInfo = startInfo};
            }
            p.WaitForExit();
            p.Close();
        }

        /// <summary>Reverse complement a nucleotide sequence. Input string will be uppercased.</summary>
        /// <param name="str">Nucleotide sequence to reverse complement.</param>
        /// <returns>Reverse complemented nucleotide sequence.</returns>
        public static string ReverseComplement(string str)
        {
            string tmp = Complement(Reverse(str));
            return tmp;
        }

        /// <summary>Reverse a string.</summary>
        /// <param name="str">String to reverse</param>
        /// <returns>Reversed string.</returns>
        private static string Reverse(string str)
        {
            char[] chrArray = str.ToCharArray();
            Array.Reverse(chrArray);
            return new string(chrArray);
        }

        /// <summary>Get the complement of the nucleotide sequence.</summary>
        /// <param name="str">Nucleotide sequence</param>
        /// <returns>Complemented nucleotide sequence</returns>
        private static string Complement(string str)
        {
            char[] ch = str.ToUpper().ToCharArray();
            for (int i = 0; i < ch.Length; i++)
            {
                switch (ch[i])
                {
                    case 'A':
                        ch[i] = 'T';
                        break;
                    case 'G':
                        ch[i] = 'C';
                        break;
                    case 'C':
                        ch[i] = 'G';
                        break;
                    case 'T':
                        ch[i] = 'A';
                        break;
                }
            }
            return new string(ch);
        }

        /// <summary>Get the file path string for an extra test info file if it exists.</summary>
        /// <param name="testname">Test name without extensions. Just the name of the test.</param>
        /// <returns></returns>
        public static string GetExtraTestInfoFilePath(string testname)
        {
            string executablePath = Application.ExecutablePath;
            var exeFileInfo = new FileInfo(executablePath);
            DirectoryInfo dirExe = exeFileInfo.Directory;
            if (dirExe != null)
            {
                FileInfo[] fileInfos = dirExe.GetFiles(testname + ".txt", SearchOption.AllDirectories);
                if (fileInfos.Length > 0)
                {
                    return fileInfos[0].FullName;
                }
            }
            return "";
        }


        public static List<Color> GetQualColours(int n)
        {

            // 19 - 21 colours Qualitative colour schemes by Paul Tol
            if (n >= 19)
            {
                return new List<Color>
                           {
                               ColorTranslator.FromHtml("#771155"),
                               ColorTranslator.FromHtml("#AA4488"),
                               ColorTranslator.FromHtml("#CC99BB"),
                               ColorTranslator.FromHtml("#114477"),
                               ColorTranslator.FromHtml("#4477AA"),
                               ColorTranslator.FromHtml("#77AADD"),
                               ColorTranslator.FromHtml("#117777"),
                               ColorTranslator.FromHtml("#44AAAA"),
                               ColorTranslator.FromHtml("#77CCCC"),
                               ColorTranslator.FromHtml("#117744"),
                               ColorTranslator.FromHtml("#44AA77"),
                               ColorTranslator.FromHtml("#88CCAA"),
                               ColorTranslator.FromHtml("#777711"),
                               ColorTranslator.FromHtml("#AAAA44"),
                               ColorTranslator.FromHtml("#DDDD77"),
                               ColorTranslator.FromHtml("#774411"),
                               ColorTranslator.FromHtml("#AA7744"),
                               ColorTranslator.FromHtml("#DDAA77"),
                               ColorTranslator.FromHtml("#771122"),
                               ColorTranslator.FromHtml("#AA4455"),
                               ColorTranslator.FromHtml("#DD7788"),
                           };
            }
            if (n >= 16)
            {
                // 16 - 18 colours Qualitative colour schemes by Paul Tol
                return new List<Color>
                           {
                               ColorTranslator.FromHtml("#771155"),
                               ColorTranslator.FromHtml("#AA4488"),
                               ColorTranslator.FromHtml("#CC99BB"),
                               ColorTranslator.FromHtml("#114477"),
                               ColorTranslator.FromHtml("#4477AA"),
                               ColorTranslator.FromHtml("#77AADD"),
                               ColorTranslator.FromHtml("#117777"),
                               ColorTranslator.FromHtml("#44AAAA"),
                               ColorTranslator.FromHtml("#77CCCC"),
                               ColorTranslator.FromHtml("#777711"),
                               ColorTranslator.FromHtml("#AAAA44"),
                               ColorTranslator.FromHtml("#DDDD77"),
                               ColorTranslator.FromHtml("#774411"),
                               ColorTranslator.FromHtml("#AA7744"),
                               ColorTranslator.FromHtml("#DDAA77"),
                               ColorTranslator.FromHtml("#771122"),
                               ColorTranslator.FromHtml("#AA4455"),
                               ColorTranslator.FromHtml("#DD7788"),
                           };
            }
            if (n == 15)
            {
                // 15 colours Qualitative colour schemes by Paul Tol
                return new List<Color>
                           {
                               ColorTranslator.FromHtml("#114477"),
                               ColorTranslator.FromHtml("#4477AA"),
                               ColorTranslator.FromHtml("#77AADD"),
                               ColorTranslator.FromHtml("#117755"),
                               ColorTranslator.FromHtml("#44AA88"),
                               ColorTranslator.FromHtml("#99CCBB"),
                               ColorTranslator.FromHtml("#777711"),
                               ColorTranslator.FromHtml("#AAAA44"),
                               ColorTranslator.FromHtml("#DDDD77"),
                               ColorTranslator.FromHtml("#771111"),
                               ColorTranslator.FromHtml("#AA4444"),
                               ColorTranslator.FromHtml("#DD7777"),
                               ColorTranslator.FromHtml("#771144"),
                               ColorTranslator.FromHtml("#AA4477"),
                               ColorTranslator.FromHtml("#DD77AA"),
                           };
            }
            if (n >= 13)
            {
                // 13 - 14 colours Qualitative colour schemes by Paul Tol
                return new List<Color>
                           {
                               ColorTranslator.FromHtml("#882E72"),
                               ColorTranslator.FromHtml("#B178A6"),
                               ColorTranslator.FromHtml("#D6C1DE"),
                               ColorTranslator.FromHtml("#1965B0"),
                               ColorTranslator.FromHtml("#5289C7"),
                               ColorTranslator.FromHtml("#7BAFDE"),
                               ColorTranslator.FromHtml("#4EB265"),
                               ColorTranslator.FromHtml("#90C987"),
                               ColorTranslator.FromHtml("#CAE0AB"),
                               ColorTranslator.FromHtml("#F7EE55"),
                               ColorTranslator.FromHtml("#F6C141"),
                               ColorTranslator.FromHtml("#F1932D"),
                               ColorTranslator.FromHtml("#E8601C"),
                               ColorTranslator.FromHtml("#DC050C"),
                           };
            }
            if (n >= 10)
            {
                // 10 - 12 colours - ColorBrewer Paired
                return new List<Color>
                           {
                               ColorTranslator.FromHtml("#A6CEE3"),
                               ColorTranslator.FromHtml("#1F78B4"),
                               ColorTranslator.FromHtml("#B2DF8A"),
                               ColorTranslator.FromHtml("#33A02C"),
                               ColorTranslator.FromHtml("#FB9A99"),
                               ColorTranslator.FromHtml("#E31A1C"),
                               ColorTranslator.FromHtml("#FDBF6F"),
                               ColorTranslator.FromHtml("#FF7F00"),
                               ColorTranslator.FromHtml("#CAB2D6"),
                               ColorTranslator.FromHtml("#6A3D9A"),
                               ColorTranslator.FromHtml("#FFFF99"),
                               ColorTranslator.FromHtml("#B15928"),
                           };
            }
            // 1 - 9 colours - ColorBrewer Set1
            return new List<Color>
                       {
                           ColorTranslator.FromHtml("#FBB4AE"),
                           ColorTranslator.FromHtml("#B3CDE3"),
                           ColorTranslator.FromHtml("#CCEBC5"),
                           ColorTranslator.FromHtml("#DECBE4"),
                           ColorTranslator.FromHtml("#FED9A6"),
                           ColorTranslator.FromHtml("#FFFFCC"),
                           ColorTranslator.FromHtml("#E5D8BD"),
                           ColorTranslator.FromHtml("#FDDAEC"),
                           ColorTranslator.FromHtml("#F2F2F2"),
                       };
        }


        public static List<Color> GetQualColourWithAlphaApplied(int alphaValue, int n)
        {
            var colorList = new List<Color>();
            foreach (var color in Misc.GetQualColours(n))
            {
                colorList.Add(Color.FromArgb(alphaValue, color));
            }
            return colorList;
        }
    }
}