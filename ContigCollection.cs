﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace MicrobialInSilicoTyper
{
    /// <summary>Holds all marker matching information for a multifasta file containing fasta entries/contigs.</summary>
    [Serializable]
    public class ContigCollection
    {
        /// <summary>In silico typing analysis.</summary>
        private readonly InSilicoTyping _ist;

        /// <summary>List of marker matches.</summary>
        private List<MarkerMatch> _markerMatches;

        /// <summary>Marker with marker matches</summary>
        private Dictionary<Marker, MarkerMatch> _markerMatchesDict;

        /// <summary>List of Marker.</summary>
        private readonly List<Marker> _markers;

        /// <summary>List of contigs or fasta entries.</summary>
        private List<Contig> _contigs;

        /// <summary>Multifasta file file info.</summary>
        private readonly FileInfo _filename;

        /// <summary>Dictionary for header names and header objects.</summary>
        private Dictionary<string, Contig> _headerContigDict;

        /// <summary>If the DNA sequences have been cleared (nulled) in the contig objects.</summary>
        private bool _sequencesCleared = true;

        /// <summary>Create new contig collection from multifasta file.</summary>
        /// <param name="ist">In silico typing analysis.</param>
        /// <param name="filename"></param>
        public ContigCollection(InSilicoTyping ist, string filename)
        {
            _ist = ist;
            _filename = new FileInfo(filename);
            _markers = _ist.Markers;
            _markerMatches = new List<MarkerMatch>(_markers.Count);
            _markerMatchesDict = new Dictionary<Marker, MarkerMatch>(_markers.Count);
        }

        public ContigCollection(string filename)
        {
            _filename = new FileInfo(filename);
        }

        private ContigCollection()
        {
            _contigs = new List<Contig>();
        }

        public Dictionary<Marker, MarkerMatch> MarkerMatchesDict { get { return _markerMatchesDict; } set { _markerMatchesDict = value; } }

        public Dictionary<string, Contig> HeaderContigDict { get { return _headerContigDict; } set { _headerContigDict = value; } }

        public List<Contig> Contigs { get { return _contigs; } set { _contigs = value; } }

        public string Name { get { return _filename.Name.Remove(_filename.Name.LastIndexOf('.')); } }

        public List<MarkerMatch> MarkerMatches { get { return _markerMatches; } set { _markerMatches = value; } }

        /// <summary>Read multifasta file into contig objects. Headers substituted for numbers starting at 0 and incrementing with each contig.</summary>
        /// <returns></returns>
        public Exception Read()
        {
            try
            {
                _contigs = new List<Contig>();
                _headerContigDict = new Dictionary<string, Contig>();
                using (var sr = new StreamReader(_filename.FullName))
                {
                    string readLine = sr.ReadLine();
                    int count = 0;
                    if (readLine != null)
                    {
                        string header = readLine.Replace(">", "").Replace(' ', '_').Replace('|', '_');
                        var sb = new StringBuilder();
                        Contig contig;
                        while (!sr.EndOfStream)
                        {
                            string tmp = sr.ReadLine();
                            if (tmp != null && tmp.Contains(">"))
                            {
                                if (sb.Length > 0)
                                {
                                    var seq = sb.ToString();

                                    contig = new Contig(header, count, seq, this);

                                    _contigs.Add(contig);
                                    _headerContigDict.Add(contig.Header, contig);
                                    header = tmp.Replace(">", "").Replace(' ', '_').Replace('|', '_');
                                    sb.Clear();
                                    count++; //increment contig index
                                }
                            }
                            else
                            {
                                if (tmp != null)
                                {
                                    sb.Append(tmp.ToUpper());
                                }
                            }
                        }
                        if (sb.Length > 0)
                        {
                            var seq = sb.ToString();
                            contig = new Contig(header, count, seq, this);
                            _contigs.Add(contig);
                            _headerContigDict.Add(contig.Header, contig);
                        }
                    }
                }
                _sequencesCleared = false;
            }
            catch (Exception ex)
            {
                return ex;
            }
            return null;
        }

        /// <summary>Get all the primer matches for each contig within the contig collection (multiFasta file) and set the contig sequences to null (clear memory).</summary>
        /// <exception cref="NotImplementedException"></exception>
        public void GetMarkerMatchesSingleCore()
        {
            //see if it's necessary to read the file and create a database for BLAST
            //only read file and create BLAST DB if marker results are missing
            bool missingResults = false;
            foreach (Marker marker in _markers)
            {
                //if the marker has not been selected to be run then move onto the next one
                if (!marker.Selected)
                {
                    continue;
                }

                //if the marker test has already been completed, move onto the next one
                if (_markerMatchesDict.ContainsKey(marker))
                {
                    continue;
                }
                missingResults = true;
                break;
            }
            if (!missingResults)
            {
                return;
            }
            if (_sequencesCleared)
            {
                //read the file and get the contig sequences again
                Read();
            }
            //get the file info for the multifasta file
            //that is going to be blasted against the allele "database" file (actually a multifasta file)
            var subjectFile = new FileInfo(_filename.FullName);

            DirectoryInfo tempDir = Misc.TempDir;
            int numFastaEntries;
            //write the temp multifasta file with fixed headers; spaces not compatible with blastall or formatdb
            if (!File.Exists(Path.Combine(tempDir.FullName, subjectFile.Name)))
            {
                subjectFile = Misc.WriteTempMultifastaFile(subjectFile, tempDir, out numFastaEntries);
            }

            //run makeblastdb first to make the db if it hasn't been made yet so check if the db files are present
            //extensions are nin, nhr, nsq added to the file that the blast db was made from
            if (!File.Exists(string.Format("{0}.nin", subjectFile.FullName)) || !File.Exists(string.Format("{0}.nhr", subjectFile.FullName)) ||
                !File.Exists(string.Format("{0}.nsq", subjectFile.FullName)))
            {
                Misc.MakeBlastDB(tempDir, subjectFile);
            }


            var probeMarkers = new List<Marker>();
            var snpMarkers = new List<Marker>();
            var alleleMarkers = new List<Marker>();
            foreach (Marker marker in _markers)
            {
                //if the marker has not been selected to be run then move onto the next one
                if (!marker.Selected)
                {
                    continue;
                }

                //if the marker test has already been completed, move onto the next one
                if (_markerMatchesDict.ContainsKey(marker))
                {
                    continue;
                }

                switch (marker.TypingTest)
                {
                    case TestType.Allelic:
                        alleleMarkers.Add(marker);
                        //Marker marker2 = marker;
                        //GetAlleleMatch(marker2, tempDir, subjectFile);
                        break;
                    case TestType.AmpliconProbe:
                    case TestType.OligoProbe:
                        probeMarkers.Add(marker);
                        break;
                    case TestType.Repeat:
                    case TestType.PCR:
                        Marker marker1 = marker;
                        GetPCRMatch(marker1, tempDir, subjectFile);
                        break;
                    case TestType.SNP:
                        snpMarkers.Add(marker);
                        break;
                    default: //typing test not yet implemented
                        throw new NotImplementedException();
                }
            }

            if (probeMarkers.Count > 0)
            {
                GetProbeMatches(probeMarkers, tempDir, subjectFile);
            }

            if (snpMarkers.Count > 0)
            {
                GetSNPProbeMatches(snpMarkers, tempDir, subjectFile);
            }
            if (alleleMarkers.Count > 0)
            {
                GetAlleleMarkerMatches(alleleMarkers, tempDir, subjectFile);
            }



            //check that matches have been found for all of the markers
            foreach (Marker marker in _markers)
            {
                //if the marker has not been selected to be run then move onto the next one
                if (!marker.Selected)
                {
                    continue;
                }

                //if the marker test has already been completed, move onto the next one
                if (_markerMatchesDict.ContainsKey(marker))
                {
                    continue;
                }
                GetMarkerMatchesSingleCore();
            }



            //no need to keep entire contig sequences in memory anymore
            foreach (Contig contig in _contigs)
            {
                contig.ClearSequence();
            }
            _sequencesCleared = true;
        }

        private void GetAlleleMarkerMatches(List<Marker> alleleMarkers, DirectoryInfo tempDir, FileInfo subjectFile)
        {
            var query = new ContigCollection();
            var queryMarkerDict = new Dictionary<Contig, Marker>();
            var headerDict = new Dictionary<string, string>();
            var headerToContig = new Dictionary<string, Contig>();
            int markerIdx = 0;
            var seqIdx = 0;
            foreach (Marker marker in alleleMarkers)
            {
                var allelesFile = new FileInfo(marker.AllelicDatabaseFilename);
                var alleles = new ContigCollection(allelesFile.FullName);
                alleles.Read();
                var alleleIdx = 0;
                foreach (Contig contig1 in alleles.Contigs)
                {
                    var newHeader = string.Format("{0}-{1}", markerIdx, alleleIdx);
                    var contig = new Contig(newHeader, seqIdx, contig1.Sequence, query);

                    query._contigs.Add(contig);
                    queryMarkerDict.Add(contig, marker);
                    headerDict.Add(newHeader, contig1.Header);
                    headerToContig.Add(newHeader, contig);

                    alleleIdx++;
                    seqIdx++;
                }
                markerIdx++;
            }

            var markerBlastOutput = new Dictionary<Marker, List<BlastOutput>>();
            IEnumerable<BlastOutput> blastOutput = RunBlast(tempDir, subjectFile, query._contigs, TestType.Allelic);
            foreach (BlastOutput bo in blastOutput)
            {
                string queryName = bo.QueryName;
                Contig contig = headerToContig[queryName];
                Marker marker = queryMarkerDict[contig];
                if (markerBlastOutput.ContainsKey(marker))
                {
                    markerBlastOutput[marker].Add(bo);
                }
                else
                {
                    markerBlastOutput.Add(marker, new List<BlastOutput> { bo });
                }
            }

            foreach (var p in markerBlastOutput)
            {
                var marker = p.Key;
                var blastOutputs = p.Value;
                var longestPerfectMatches = blastOutputs
                    // filter for blast matches that are full length and 100% identity
                    .Where(b => b.AlignmentLength == b.QueryLength && Math.Abs(b.PercentIdentity - 100.0) < double.Epsilon)
                    .OrderByDescending(perfectMatch => perfectMatch.AlignmentLength)
                    .ToList();
                BlastOutput bo;
                bo = longestPerfectMatches.Count > 0 ?
                    longestPerfectMatches.First() :
                    blastOutputs.OrderByDescending(b => b.BitScore).First();
                MarkerMatch markerMatch = GetAlleleMarkerMatch(marker, bo, headerDict);
                _markerMatches.Add(markerMatch);
                _markerMatchesDict.Add(marker, markerMatch);
            }
            foreach (var marker in alleleMarkers)
            {
                if (!_markerMatchesDict.ContainsKey(marker))
                {
                    var mm = new MarkerMatch(null, string.Empty, null, this, string.Empty, false, marker, -1, -1, -1);
                    _markerMatches.Add(mm);
                    _markerMatchesDict.Add(marker, mm);
                }
            }
        }


        /// <summary>Find the binding sites for each SNP probe marker using BLAST.</summary>
        /// <param name="markers">List of SNP probe markers.</param>
        /// <param name="tempDir">Temporary working directory for BLAST.</param>
        /// <param name="subjectFile">Subject genome file.</param>
        private void GetSNPProbeMatches(List<Marker> markers, DirectoryInfo tempDir, FileInfo subjectFile)
        {
            //write probe sequence to file in multifasta format
            var query = new ContigCollection();
            var queryMarkerDict = new Dictionary<Contig, Marker>();
            int count = 0;
            foreach (Marker marker in markers)
            {
                var contig = new Contig(marker.Name, count, marker.ForwardPrimer, query);
                query._contigs.Add(contig);
                queryMarkerDict.Add(contig, marker);
                count++;
            }

            IEnumerable<BlastOutput> blastOutput = RunBlast(tempDir, subjectFile, query._contigs, TestType.SNP);
            //read the BLAST output file and get the top hit according to e-value
            //get the adjusted subject genome indices for the allele match;
            //how much to add to the start and end of the match


            foreach (BlastOutput bo in blastOutput)
            {
                if (bo == null)
                {
                    throw new Exception(string.Format("Null BlastOutput for SNP probe match."));
                }
                Marker marker = queryMarkerDict[query._contigs[Convert.ToInt32(bo.QueryName)]];
                var queryEndIndex = bo.QueryEndIndex;
                var queryStartIndex = bo.QueryStartIndex;
                var subjectEndIndex = bo.SubjectEndIndex;
                var subjectStartIndex = bo.SubjectStartIndex;

                Misc.AdjustSubjectIndices(bo.QueryLength, ref queryEndIndex, ref queryStartIndex, bo.ReverseComplement, ref subjectEndIndex, ref subjectStartIndex);
                //get the contig match
                Contig contig = null;
                if (bo.SubjectName != "")
                {
                    int contigIndex;
                    if (int.TryParse(bo.SubjectName, out contigIndex))
                    {
                        contig = _contigs[contigIndex];
                    }
                }
                //if there is no contig match then return null and an error message should be triggered
                if (contig == null)
                {
                    var match = new MarkerMatch(marker, this);
                    _markerMatches.Add(match);
                    _markerMatchesDict.Add(marker, match);
                    continue;
                }
                //get the amplicon sequence with the adjusted start and end indices
                string amplicon = bo.SubjAln;
                //if there is a contig truncation then just set the number of mismatches as the amplicon length - can't be sure what the true number of mismatches is
                var contigTruncation = Misc.GetAmplicon(subjectStartIndex,
                                  subjectEndIndex,
                                  bo.ReverseComplement,
                                  contig,
                                  ref amplicon);
                int mismatches = contigTruncation
                                     ? bo.Mismatches
                                     : Misc.GetDifferences(marker.ForwardPrimer, amplicon);

                var markerMatch = new MarkerMatch(bo,
                    bo.QueryName,
                    contig,
                    this,
                    amplicon,
                    contigTruncation,
                    marker,
                    mismatches,
                    subjectEndIndex,
                    subjectStartIndex);
                if (_markerMatchesDict.ContainsKey(marker))
                {
                    continue;
                }

                _markerMatches.Add(markerMatch);
                _markerMatchesDict.Add(marker, markerMatch);
            }

            if (_markerMatchesDict.Count != markers.Count)
            {
                foreach (Marker marker in markers)
                {
                    if (_markerMatchesDict.ContainsKey(marker))
                    {
                        continue;
                    }
                    var markerMatch = new MarkerMatch(marker, this);
                    _markerMatches.Add(markerMatch);
                    _markerMatchesDict.Add(marker, markerMatch);
                }
            }
        }


        /// <summary>Find the binding sites for each probe marker using BLAST.</summary>
        /// <param name="markers">List of probe markers.</param>
        /// <param name="tempDir">Temporary working directory for BLAST.</param>
        /// <param name="subjectFile">Subject genome file.</param>
        private void GetProbeMatches(List<Marker> markers, DirectoryInfo tempDir, FileInfo subjectFile)
        {
            //write probe sequence to file in multifasta format
            List<Contig> probes;
            Dictionary<Contig, Marker> queryMarkerDict;
            Dictionary<string, Marker> queryHeaderMarker;
            GetProbeQuerySequencesForBlast(markers, out queryMarkerDict, out queryHeaderMarker, out probes);

            IEnumerable<BlastOutput> blastOutput = RunBlast(tempDir, subjectFile, probes, TestType.OligoProbe);
            //read the BLAST output file and get the top hit according to e-value
            //get the adjusted subject genome indices for the allele match;
            //how much to add to the start and end of the match

            foreach (BlastOutput bo in blastOutput)
            {
                if (bo == null)
                {
                    throw new Exception(string.Format("Null BlastOutput for probe match."));
                }

                Marker marker = queryHeaderMarker[bo.QueryName];

                Contig contig = GetSubjectContig(bo);

                if (contig == null)
                {
                    if (!_markerMatchesDict.ContainsKey(marker))
                    {
                        var match = new MarkerMatch(marker, this);
                        _markerMatchesDict.Add(marker, match);
                    }
                    continue;
                }
                if (_markerMatchesDict.ContainsKey(marker))
                {
                    if (!string.IsNullOrEmpty(_markerMatchesDict[marker].Amplicon))
                    {
                        continue;
                    }
                }
                var queryEndIndex = bo.QueryEndIndex;
                var queryStartIndex = bo.QueryStartIndex;
                var subjectEndIndex = bo.SubjectEndIndex;
                var subjectStartIndex = bo.SubjectStartIndex;

                Misc.AdjustSubjectIndices(bo.QueryLength, ref queryEndIndex, ref queryStartIndex, bo.ReverseComplement, ref subjectEndIndex, ref subjectStartIndex);
                //get the amplicon sequence with the adjusted start and end indices
                string amplicon = bo.SubjAln;
                int mismatches;
                bool contigTruncation;
                if (Misc.IsDegenSequence(marker.ForwardPrimer))
                {
                    contigTruncation = Misc.GetAmplicon(subjectStartIndex,
                        subjectEndIndex,
                        bo.ReverseComplement,
                        contig,
                        ref amplicon);
                    mismatches = contigTruncation
                                     ? bo.Mismatches
                                     : marker.ForwardPrimer.Length == amplicon.Length
                                           ? GetBestDegenSeqMatch(marker.ForwardPrimer, amplicon)
                                           : marker.ForwardPrimer.Length;
                }
                else
                {
                    //if there is a contig truncation then just set the number of mismatches as the amplicon length - can't be sure what the true number of mismatches is
                    contigTruncation = Misc.GetAmplicon(subjectStartIndex,
                        subjectEndIndex,
                        bo.ReverseComplement,
                        contig,
                        ref amplicon);
                    mismatches = contigTruncation
                                     ? bo.Mismatches
                                     : Misc.GetDifferences(marker.ForwardPrimer, amplicon);
                }


                var markerMatch = new MarkerMatch(bo,
                    bo.QueryName,
                    contig,
                    this,
                    amplicon,
                    contigTruncation,
                    marker,
                    mismatches,
                    subjectEndIndex,
                    subjectStartIndex);
                if (_markerMatchesDict.ContainsKey(marker))
                {
                    //if the match for the current marker already exists in the dict then check if it's a better match
                    MarkerMatch match = _markerMatchesDict[marker];
                    if (markerMatch.Mismatches < match.Mismatches)
                    {
                        _markerMatchesDict[marker] = markerMatch;
                    }
                }
                else
                {
                    //if the marker-marker match dict doesn't contain a match for the current marker, add it to the dict
                    _markerMatchesDict.Add(marker, markerMatch);
                }
            }

            foreach (Marker marker in markers)
            {
                if (!_markerMatchesDict.ContainsKey(marker))
                {
                    var markerMatch = new MarkerMatch(marker, this);
                    _markerMatchesDict.Add(marker, markerMatch);    
                }
            }

            //populate marker match list
            foreach (var pair in _markerMatchesDict)
            {
                _markerMatches.Add(pair.Value);
            }
        }

        private static void GetProbeQuerySequencesForBlast(
            IEnumerable<Marker> markers, 
            out Dictionary<Contig, Marker> queryMarkerDict, 
            out Dictionary<string, Marker> queryHeaderMarker, 
            out List<Contig> probes)
        {

            probes = new List<Contig>();
            queryMarkerDict = new Dictionary<Contig, Marker>();
            queryHeaderMarker = new Dictionary<string, Marker>();
            int count = 0;

            foreach (Marker marker in markers)
            {
                //if the probe is degenerate, expand sequence into all non-degenerate sequence possibilities
                if (Misc.IsDegenSequence(marker.ForwardPrimer))
                {
                    var degenProbeExpanded = new List<string>();
                    Misc.ExpandDegenSequence(new char[marker.ForwardPrimer.Length], marker.ForwardPrimer, 0, degenProbeExpanded);
                    foreach (string probeSeq in degenProbeExpanded)
                    {
                        var contig = new Contig(count.ToString(CultureInfo.InvariantCulture), count, probeSeq, null);
                        queryHeaderMarker.Add(contig.Header, marker);
                        queryMarkerDict.Add(contig, marker);
                        probes.Add(contig);
                        count++;
                    }
                }
                else
                {
                    var contig = new Contig(count.ToString(CultureInfo.InvariantCulture), count, marker.ForwardPrimer, null);
                    queryHeaderMarker.Add(contig.Header, marker);
                    queryMarkerDict.Add(contig, marker);
                    probes.Add(contig);
                    count++;
                }
            }
        }

        private Contig GetSubjectContig(BlastOutput bo)
        {
            Contig contig = null;
            if (bo.SubjectName != "")
            {
                int contigIndex;
                if (int.TryParse(bo.SubjectName, out contigIndex))
                {
                    contig = _contigs[contigIndex];
                }
            }
            return contig;
        }

        private int GetBestDegenSeqMatch(string marker, string amplicon)
        {
            var list = new List<string>();
            Misc.ExpandDegenSequence(new char[marker.Length], marker, 0, list);

            //try finding match using hashset
            var hash = new HashSet<string>();
            foreach (string s in list)
            {
                hash.Add(s);
            }
            if (hash.Contains(amplicon))
            {
                return 0;
            }

            int leastMismatches = int.MaxValue;

            foreach (string s in list)
            {
                int mismatches = 0;
                int count = 0;
                foreach (char c in s)
                {
                    if (c != amplicon[count])
                    {
                        mismatches++;
                    }
                    count++;
                }
                if (mismatches < leastMismatches)
                {
                    leastMismatches = mismatches;
                }
                if (mismatches == 0)
                {
                    return 0;
                }
            }
            return leastMismatches;
        }

        /// <summary>Determine the most likely PCR amplicon using BLAST to find likely PCR primer binding sites.</summary>
        /// <param name="marker">PCR/VNTR marker.</param>
        /// <param name="tempDir">Working directory for BLAST.</param>
        /// <param name="subjectFile">Subject genome file.</param>
        private void GetPCRMatch(Marker marker, DirectoryInfo tempDir, FileInfo subjectFile)
        {
            var pcrMatching = new PCRMarkerMatching(marker, this, tempDir, subjectFile.Name);
            switch (pcrMatching.MarkerMatches.Count)
            {
                case 0:
                    {
                        //null match
                        var markerMatch = new MarkerMatch(marker, this);
                        _markerMatches.Add(markerMatch);
                        _markerMatchesDict.Add(marker, markerMatch);
                    }
                    break;
                case 1:
                    {
                        MarkerMatch markerMatch = pcrMatching.MarkerMatches[0];
                        _markerMatches.Add(markerMatch);
                        _markerMatchesDict.Add(marker, markerMatch);
                    }
                    break;
                default:
                    {
                        // need to decide which marker match is the real one
                        // need to find the marker match that results in an 
                        // acceptable amplicon size and primers in different
                        // orientations; one 5'-3' and the other 3'-5' all
                        // matches will have an acceptable amplicon size, but
                        // will not be checked for primer orientation
                        MarkerMatch realMarkerMatch = null;

                        var list = new List<MarkerMatch>();

                        foreach (MarkerMatch markerMatch in pcrMatching.MarkerMatches)
                        {
                            if (markerMatch.ForwardPrimerRevComp != markerMatch.ReversePrimerRevComp)
                            {
                                list.Add(markerMatch);
                            }
                        }
                        // if there are multiple matches with the proper primer
                        // orientation, then check which one is the most likely
                        // i.e. closest size to the expected amplicon size
                        if (list.Count > 1)
                        {
                            int minDiffObsVsExp = int.MaxValue;
                            foreach (MarkerMatch markerMatch in list)
                            {
                                int currentDiffObsVsExp = Math.Abs(markerMatch.AmpliconSize - markerMatch.ExpectedAmpliconSize);
                                if (currentDiffObsVsExp < minDiffObsVsExp)
                                {
                                    realMarkerMatch = markerMatch;
                                    minDiffObsVsExp = currentDiffObsVsExp;
                                }
                            }
                        }
                        else if (list.Count == 1)
                        {
                            realMarkerMatch = list[0];
                        }


                        //if a match cannot be found with correct primer orientations then look for the one that produces the amplicon with
                        //the least differential between expected and observed
                        if (realMarkerMatch == null)
                        {
                            int minDiffObsVsExp = int.MaxValue;
                            foreach (MarkerMatch markerMatch in pcrMatching.MarkerMatches)
                            {
                                int currentDiffObsVsExp = Math.Abs(markerMatch.AmpliconSize - markerMatch.ExpectedAmpliconSize);
                                if (currentDiffObsVsExp < minDiffObsVsExp)
                                {
                                    minDiffObsVsExp = currentDiffObsVsExp;
                                    realMarkerMatch = markerMatch;
                                }
                            }
                        }
                        _markerMatches.Add(realMarkerMatch);
                        _markerMatchesDict.Add(marker, realMarkerMatch);
                    }
                    break;
            }
        }


        private MarkerMatch GetAlleleMarkerMatch(Marker marker, BlastOutput bo, Dictionary<string, string> headerToAlleleHeader)
        {

            if (bo.SubjectName == "")
            {
                throw new Exception("Contig not found");
            }
            var contig = GetMatchingSubjectContig(marker, bo);

            string amplicon = bo.SubjAln;
            var contigTruncation = false;
            // using the length of the top BLAST hit adjust the start and end indices
            var queryEndIndex = bo.QueryEndIndex;
            var queryStartIndex = bo.QueryStartIndex;
            var subjectEndIndex = bo.SubjectEndIndex;
            var subjectStartIndex = bo.SubjectStartIndex;

            Misc.AdjustSubjectIndices(bo.QueryLength, ref queryEndIndex, ref queryStartIndex, bo.ReverseComplement, ref subjectEndIndex, ref subjectStartIndex);
            string alleleMatch = headerToAlleleHeader[bo.QueryName];
            if (bo.AlignmentLength != bo.QueryLength)
            {
                contigTruncation = Misc.GetAmplicon(subjectStartIndex, subjectEndIndex, bo.ReverseComplement, contig,
                                                    ref amplicon);
            }

            // find the closest allele match in the alleles file for that marker
            var markerMatch = new MarkerMatch(bo, alleleMatch, contig, this, amplicon, contigTruncation, marker, bo.Mismatches + bo.Gaps, subjectEndIndex, subjectStartIndex);
            if (bo.Mismatches + bo.Gaps > 0)
            {
                Console.Error.WriteLine(string.Format("No perfect match found for allele marker {0} of test {1} for multifasta {2}", marker.Name, marker.TestName, this.Name));
            }
            return markerMatch;
        }

        private Contig GetMatchingSubjectContig(Marker marker, BlastOutput bo)
        {
            int contigIndex;
            if (!int.TryParse(bo.SubjectName, out contigIndex))
            {
                throw new Exception("Contig not found"); //something messed up if null is returned here
            }
            Contig contig = _contigs[contigIndex];
            ////_headerContigDict.TryGetValue(bo.SubjectName, out contig);

            //if there is no contig match then an exception will be thrown; should always be able to retrieve the contig where the match is found
            if (contig == null)
            {
                // something messed up if null is returned here
                throw new Exception(string.Format("Contig with base-1 index {0} not found for {1}::{2}", contigIndex,
                                                  marker.Name, marker.TestName));
            }
            return contig;
        }


        /// <summary>Run BLAST with query fasta entries and the subject multifasta file to produce a list of BLAST output.</summary>
        /// <param name="workingDir">Working directory, the "Temp" folder in executable directory.</param>
        /// <param name="subjectFile">Genome sequence multifasta file. BLAST subject. BLAST DB made from this file.</param>
        /// <param name="queries">List of query fasta entries for BLAST searching.</param>
        /// <param name="testType"> </param>
        /// <returns>Blast results in BlastOuput objects</returns>
        private static IEnumerable<BlastOutput> RunBlast(DirectoryInfo workingDir, FileInfo subjectFile, IEnumerable<Contig> queries, TestType testType)
        {
            var queryString = GetQueryFastaString(queries);
            var blastprocess = new BlastProcess(workingDir, queryString, subjectFile.Name, testType);
            return blastprocess.BlastOutputs;
        }

        private static string GetQueryFastaString(IEnumerable<Contig> queries)
        {
            string queryString;
            using (var sw = new StringWriter())
            {
                foreach (Contig contig in queries)
                {
                    sw.Write(">");
                    sw.WriteLine(contig.Header);
                    sw.WriteLine(contig.Sequence);
                }
                queryString = sw.ToString();
            }
            return queryString;
        }
    }
}