﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace MicrobialInSilicoTyper
{
    static class Program
    {
        /// <summary>
        /// The MainForm entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Process.Start(new ProcessStartInfo("blastn", "-h") { RedirectStandardError = true, UseShellExecute = false });
            }
            catch (Exception)
            {
                throw new Exception(
                    "BLAST+ cannot be found. Please install NCBI BLAST+ and ensure that it is runnable from the command line.");
            }
            Misc.BlastErrorRetryLimit = 1;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
