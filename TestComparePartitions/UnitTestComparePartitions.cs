﻿using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MicrobialInSilicoTyper;

namespace TestMIST
{
    /// <summary>
    /// Test that all of the partition congruence metrics are being calculated properly. 
    /// Compare the values obtained by the ComparePartitions class to the values obtained through the ComparingPartitions website.
    /// The values for these metrics should only differ at the 0.001 level due to precision.
    /// </summary>
    [TestClass]
    public class UnitTestComparePartitions
    {

        #region comparing partitions typing method values
        Dictionary<string, List<string>> _testMethods = new Dictionary<string, List<string>>
            {
                {
                    "T type", 
                    new List<string>{"4", "12", "NT", "12", "12", "12", "4", "4", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "28", "13", "13", "12", "12", "12", "12", "4", "13", "28", "12", "12", "12", "12", "12", "4", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "13", "12", "2", "28", "2", "12", "12", "12", "12", "12", "13", "12", "12", "12", "12", "12", "12", "6", "12", "12", "12", "12", "B3264", "12", "12", "12", "12", "12", "1", "13", "1", "1", "28", "9", "9", "12", "12", "12", "12", "6", "12", "12", "12", "NT", "12", "12", "13", "13", "13", "12", "12", "12", "12", "12", "NT", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "28", "12", "12", "12", "13", "4", "12", "12", "12", "12", "12", "12", "12", "12", "12", "12", "28", "12", "12", "12", "NT", "28", "12", "4", "9", "28", "4", "12", "4", "12", "28", "28", "12", "1", "NT", "9", "12", "12", "NT", "12", "4", "28", "28", "4", "12", "12", "1", "9", "4", "28", "9", "12", "1", "12", "12", "12", "12", "12", "28", "25", "12", "1", "4", "4", "12", "25", "28", "1", "28", "1", "12", "12", "12", "1", "28", "28", "28", "28", "1", "12", "12", "1", "1", "1", "1", "12", "1", "12", "1", "B3264", "25", "25", "1", "12", "NT", "12", "4", "12", "28", "28", "13", "28", "28", "12", "28", "25", "1", "25", "1", "1", "NT", "12", "NT", "1", "1", "4", "4", "12", "4", "9", "1", "4", "4", "1", "12", "28", "1", "1", "1", "4", "1", "NT", "28", "12", "12", "25", "9", "12", "28", "12", "25", "25", "28", "28", "1", "12", "12", "12", "1", "1", "1", "1", "1", "12", "12", "NT", "25", "12", "1", "13", "5/27/1944", "12", "25", "28", "1", "2", "B3264", "4", "4", "6", "4", "NT", "12", "12", "13", "1", "1", "25", "4", "12", "1", "12", "4", "28", "2", "28", "2", "12", "4", "12", "5/27/1944", "4", "4", "B3264", "12", "4", "4", "4", "4", "25"}
                }, 
                {
                    "emm type",
                    new List<string>{ "4", "22", "22", "22", "22", "22", "4", "4", "22", "22", "22", "22", "22", "22", "22", "22", "22", "22", "22", "22", "22", "22", "2", "22", "22", "22", "22", "12", "28", "77", "11", "22", "22", "22", "22", "4", "12", "28", "22", "22", "22", "22", "22", "4", "11", "22", "11", "22", "22", "22", "22", "22", "22", "22", "22", "22", "22", "22", "22", "1", "22", "2", "28", "2", "22", "12", "22", "22", "22", "77", "22", "12", "22", "22", "12", "22", "6", "22", "12", "4", "22", "89", "22", "22", "22", "9", "22", "1", "4", "1", "1", "28", "9", "9", "22", "22", "22", "12", "75", "12", "22", "22", "12", "22", "22", "77", "22", "22", "22", "22", "22", "22", "22", "12", "12", "22", "22", "22", "22", "22", "22", "22", "22", "22", "12", "28", "22", "12", "4", "22", "4", "22", "22", "22", "22", "22", "22", "22", "22", "12", "22", "28", "22", "22", "22", "12", "28", "22", "4", "9", "28", "4", "22", "4", "22", "28", "28", "22", "1", "12", "9", "22", "12", "12", "12", "4", "28", "28", "4", "22", "12", "1", "9", "4", "28", "9", "22", "1", "22", "22", "22", "12", "22", "28", "22", "22", "1", "4", "4", "22", "75", "28", "1", "28", "1", "12", "22", "22", "1", "28", "28", "28", "28", "1", "22", "22", "1", "1", "4", "1", "22", "1", "12", "1", "89", "75", "75", "1", "22", "1", "12", "4", "22", "28", "28", "4", "28", "28", "22", "28", "75", "1", "75", "1", "1", "4", "12", "28", "1", "1", "4", "4", "12", "4", "9", "1", "4", "4", "1", "22", "28", "1", "1", "1", "4", "1", "28", "28", "22", "22", "75", "9", "12", "28", "12", "75", "75", "28", "28", "1", "22", "12", "22", "1", "1", "1", "1", "1", "22", "22", "1", "75", "22", "1", "1", "4", "22", "75", "28", "4", "12", "4", "4", "4", "4", "4", "12", "12", "12", "4", "1", "1", "75", "4", "12", "1", "22", "4", "28", "2", "28", "4", "22", "4", "22", "4", "4", "4", "4", "22", "4", "4", "4", "4", "75", }
                },
                { 
                    "PFGE Sma80", 
                    new List<string> { "8", "9", "9", "9", "9", "9", "8", "8", "9", "9", "9", "9", "9", "9", "9", "9", "9", "9", "9", "9", "9", "9", "7", "9", "9", "9", "9", "1", "11", "12", "19", "9", "9", "9", "9", "8", "1", "11", "9", "9", "9", "9", "11", "8", "19", "9", "19", "9", "9", "9", "9", "9", "9", "9", "11", "9", "9", "11", "9", "17", "9", "7", "11", "7", "9", "1", "9", "9", "9", "12", "9", "1", "9", "9", "1", "9", "21", "11", "2", "8", "9", "16", "9", "9", "9", "4", "11", "17", "8", "17", "17", "11", "5", "5", "9", "11", "11", "2", "6", "8", "9", "9", "1", "9", "9", "12", "9", "9", "9", "9", "9", "9", "9", "1", "1", "9", "9", "9", "9", "3", "9", "9", "9", "9", "2", "11", "9", "2", "8", "9", "8", "9", "9", "9", "9", "9", "9", "9", "9", "1", "9", "11", "3", "9", "9", "2", "11", "9", "8", "5", "11", "8", "9", "8", "9", "11", "11", "9", "17", "1", "5", "10", "1", "20", "20", "8", "11", "11", "8", "9", "2", "17", "5", "8", "11", "5", "9", "17", "9", "9", "9", "1", "9", "11", "9", "9", "13", "8", "8", "9", "15", "11", "17", "11", "17", "1", "9", "9", "17", "11", "11", "11", "11", "17", "9", "9", "13", "14", "8", "18", "9", "17", "1", "18", "16", "15", "15", "17", "9", "7", "2", "8", "9", "11", "11", "8", "11", "11", "9", "11", "15", "17", "15", "17", "17", "8", "1", "11", "17", "17", "8", "8", "2", "8", "5", "17", "8", "8", "17", "9", "11", "17", "17", "17", "8", "17", "11", "11", "9", "9", "15", "5", "1", "11", "1", "15", "15", "11", "11", "18", "11", "1", "9", "17", "17", "17", "17", "17", "9", "9", "17", "15", "9", "17", "17", "8", "11", "15", "11", "8", "1", "8", "8", "8", "8", "8", "1", "8", "2", "8", "17", "17", "15", "8", "2", "17", "11", "8", "11", "7", "11", "7", "9", "8", "9", "8", "8", "8", "8", "11", "8", "8", "8", "8", "15", }
                }, 
                {
                    "PFGE SFi68",
                    new List<string> { "10", "3", "13", "13", "13", "16", "10", "10", "13", "13", "13", "13", "13", "3", "13", "13", "13", "13", "13", "13", "13", "13", "9", "13", "13", "13", "13", "14", "6", "3", "13", "13", "13", "13", "13", "10", "21", "17", "13", "13", "13", "13", "17", "10", "13", "13", "13", "13", "13", "13", "13", "13", "13", "13", "13", "13", "13", "17", "13", "5", "13", "10", "17", "10", "13", "21", "13", "13", "13", "8", "13", "21", "13", "13", "21", "17", "15", "17", "21", "10", "13", "14", "13", "13", "13", "2", "17", "1", "10", "1", "1", "17", "12", "12", "13", "17", "17", "21", "11", "10", "13", "13", "21", "13", "13", "16", "13", "13", "13", "13", "13", "13", "13", "21", "21", "13", "13", "16", "13", "13", "13", "13", "13", "13", "21", "17", "13", "21", "10", "13", "10", "13", "17", "13", "13", "13", "13", "13", "13", "21", "13", "17", "13", "13", "13", "21", "17", "13", "10", "12", "17", "10", "17", "10", "13", "17", "17", "13", "21", "21", "12", "8", "4", "18", "19", "16", "17", "17", "16", "13", "21", "1", "12", "16", "17", "3", "13", "1", "13", "13", "13", "21", "13", "17", "13", "13", "1", "10", "10", "3", "10", "17", "1", "17", "1", "21", "13", "13", "1", "17", "17", "17", "17", "5", "13", "13", "1", "20", "10", "1", "13", "1", "13", "1", "14", "10", "21", "1", "13", "5", "21", "10", "13", "17", "17", "10", "17", "17", "13", "17", "10", "1", "10", "1", "1", "10", "15", "6", "1", "1", "10", "10", "21", "10", "12", "1", "10", "10", "10", "13", "16", "1", "1", "1", "10", "1", "17", "17", "13", "13", "16", "12", "21", "17", "21", "10", "10", "17", "16", "1", "17", "21", "13", "1", "1", "1", "1", "1", "16", "13", "1", "10", "13", "1", "1", "10", "16", "10", "17", "10", "21", "10", "10", "10", "10", "10", "21", "10", "21", "10", "1", "1", "10", "10", "21", "1", "13", "10", "17", "11", "17", "11", "13", "7", "13", "10", "10", "10", "10", "17", "7", "6", "6", "7", "10", }
                },
                {
                    "T type + emm type",
                    new List<string> { "44", "1222", "NT22", "1222", "1222", "1222", "44", "44", "1222", "1222", "1222", "1222", "1222", "1222", "1222", "1222", "1222", "1222", "1222", "1222", "1222", "1222", "122", "1222", "1222", "1222", "1222", "1212", "2828", "1377", "1311", "1222", "1222", "1222", "1222", "44", "1312", "2828", "1222", "1222", "1222", "1222", "1222", "44", "1211", "1222", "1211", "1222", "1222", "1222", "1222", "1222", "1222", "1222", "1222", "1222", "1222", "1222", "1222", "131", "1222", "22", "2828", "22", "1222", "1212", "1222", "1222", "1222", "1377", "1222", "1212", "1222", "1222", "1212", "1222", "66", "1222", "1212", "124", "1222", "B326489", "1222", "1222", "1222", "129", "1222", "11", "134", "11", "11", "2828", "99", "99", "1222", "1222", "1222", "1212", "675", "1212", "1222", "1222", "NT12", "1222", "1222", "1377", "1322", "1322", "1222", "1222", "1222", "1222", "1222", "NT12", "1212", "1222", "1222", "1222", "1222", "1222", "1222", "1222", "1222", "1222", "1212", "2828", "1222", "1212", "124", "1322", "44", "1222", "1222", "1222", "1222", "1222", "1222", "1222", "1222", "1212", "1222", "2828", "1222", "1222", "1222", "NT12", "2828", "1222", "44", "99", "2828", "44", "1222", "44", "1222", "2828", "2828", "1222", "11", "NT12", "99", "1222", "1212", "NT12", "1212", "44", "2828", "2828", "44", "1222", "1212", "11", "99", "44", "2828", "99", "1222", "11", "1222", "1222", "1222", "1212", "1222", "2828", "2522", "1222", "11", "44", "44", "1222", "2575", "2828", "11", "2828", "11", "1212", "1222", "1222", "11", "2828", "2828", "2828", "2828", "11", "1222", "1222", "11", "11", "14", "11", "1222", "11", "1212", "11", "B326489", "2575", "2575", "11", "1222", "NT1", "1212", "44", "1222", "2828", "2828", "134", "2828", "2828", "1222", "2828", "2575", "11", "2575", "11", "11", "NT4", "1212", "NT28", "11", "11", "44", "44", "1212", "44", "99", "11", "44", "44", "11", "1222", "2828", "11", "11", "11", "44", "11", "NT28", "2828", "1222", "1222", "2575", "99", "1212", "2828", "1212", "2575", "2575", "2828", "2828", "11", "1222", "1212", "1222", "11", "11", "11", "11", "11", "1222", "1222", "NT1", "2575", "1222", "11", "131", "5/27/444", "1222", "2575", "2828", "14", "212", "B32644", "44", "44", "64", "44", "NT12", "1212", "1212", "134", "11", "11", "2575", "44", "1212", "11", "1222", "44", "2828", "22", "2828", "24", "1222", "44", "1222", "5/27/444", "44", "44", "B32644", "1222", "44", "44", "44", "44", "2575", }
                },
            };
        #endregion


        #region comparing partitions results

        Dictionary<string, double> _methodSimpsons = new Dictionary<string, double> { { "T type", 0.721 }, { "emm type", 0.775 }, { "PFGE Sma80", 0.810 }, { "PFGE SFi68", 0.807 }, { "T type + emm type", 0.806 } };
        Dictionary<string, double> _methodSimpsonsLower = new Dictionary<string, double> { { "T type", 0.676 }, { "emm type", 0.741 }, { "PFGE Sma80", 0.780 }, { "PFGE SFi68", 0.778 }, { "T type + emm type", 0.771 } };
        Dictionary<string, double> _methodSimpsonsUpper = new Dictionary<string, double> { { "T type", 0.767 }, { "emm type", 0.808 }, { "PFGE Sma80", 0.840 }, { "PFGE SFi68", 0.836 }, { "T type + emm type", 0.841 } };
        Dictionary<string, Dictionary<string, double>> _methodRand = new Dictionary<string, Dictionary<string, double>> 
            {
	            {
	            "emm type", new Dictionary<string, double>
		            {
			            { "T type", 0.884 },
			            { "T type + emm type", 0.969 },
			            { "PFGE SFi68", 0.909 },
			            { "PFGE Sma80", 0.946 },
		            }
	            },
	            {
	            "T type", new Dictionary<string, double>
		            {
			            { "emm type", 0.884 },
			            { "T type + emm type", 0.915 },
			            { "PFGE SFi68", 0.823 },
			            { "PFGE Sma80", 0.842 },
		            }
	            },
	            {
	            "T type + emm type", new Dictionary<string, double>
		            {
			            { "emm type", 0.969 },
			            { "T type", 0.915 },
			            { "PFGE SFi68", 0.895 },
			            { "PFGE Sma80", 0.927 },
		            }
	            },
	            {
	            "PFGE SFi68", new Dictionary<string, double>
		            {
			            { "emm type", 0.909 },
			            { "T type", 0.823 },
			            { "T type + emm type", 0.895 },
			            { "PFGE Sma80", 0.926 },
		            }
	            },
	            {
	            "PFGE Sma80", new Dictionary<string, double>
		            {
			            { "emm type", 0.946 },
			            { "T type", 0.842 },
			            { "T type + emm type", 0.927 },
			            { "PFGE SFi68", 0.926 },
		            }
	            },
            };

        Dictionary<string, Dictionary<string, double>> _methodAdjustedRand = new Dictionary<string, Dictionary<string, double>> 
            {
	            {
	            "emm type", new Dictionary<string, double>
		            {
			            { "T type", 0.693 },
			            { "T type + emm type", 0.905 },
			            { "PFGE SFi68", 0.724 },
			            { "PFGE Sma80", 0.837 },
		            }
	            },
	            {
	            "T type", new Dictionary<string, double>
		            {
			            { "emm type", 0.693 },
			            { "T type + emm type", 0.768 },
			            { "PFGE SFi68", 0.513 },
			            { "PFGE Sma80", 0.566 },
		            }
	            },
	            {
	            "T type + emm type", new Dictionary<string, double>
		            {
			            { "emm type", 0.905 },
			            { "T type", 0.768 },
			            { "PFGE SFi68", 0.663 },
			            { "PFGE Sma80", 0.764 },
		            }
	            },
	            {
	            "PFGE SFi68", new Dictionary<string, double>
		            {
			            { "emm type", 0.724 },
			            { "T type", 0.513 },
			            { "T type + emm type", 0.663 },
			            { "PFGE Sma80", 0.762 },
		            }
	            },
	            {
	            "PFGE Sma80", new Dictionary<string, double>
		            {
			            { "emm type", 0.837 },
			            { "T type", 0.566 },
			            { "T type + emm type", 0.764 },
			            { "PFGE SFi68", 0.762 },
		            }
	            },
            };

        Dictionary<string, Dictionary<string, string>> _methodAdjustedWallace = new Dictionary<string, Dictionary<string, string>> 
            {
	            {
	            "emm type", new Dictionary<string, string>
		            {
			            { "T type", "0.608 (0.514-0.703)" },
			            { "T type + emm type", "1.000 (1.000-1.000)" },
			            { "PFGE SFi68", "0.804 (0.741-0.867)" },
			            { "PFGE Sma80", "0.937 (0.914-0.961)" },
		            }
	            },
	            {
	            "T type", new Dictionary<string, string>
		            {
			            { "emm type", "0.807 (0.736-0.877)" },
			            { "T type + emm type", "1.000 (1.000-1.000)" },
			            { "PFGE SFi68", "0.672 (0.596-0.749)" },
			            { "PFGE Sma80", "0.748 (0.672-0.824)" },
		            }
	            },
	            {
	            "T type + emm type", new Dictionary<string, string>
		            {
			            { "emm type", "0.827 (0.764-0.890)" },
			            { "T type", "0.623 (0.533-0.714)" },
			            { "PFGE SFi68", "0.666 (0.587-0.745)" },
			            { "PFGE Sma80", "0.773 (0.705-0.841)" },
		            }
	            },
	            {
	            "PFGE SFi68", new Dictionary<string, string>
		            {
			            { "emm type", "0.659 (0.566-0.752)" },
			            { "T type", "0.415 (0.316-0.514)" },
			            { "T type + emm type", "0.660 (0.559-0.760)" },
			            { "PFGE Sma80", "0.767 (0.685-0.850)" },
		            }
	            },
	            {
	            "PFGE Sma80", new Dictionary<string, string>
		            {
			            { "emm type", "0.757 (0.674-0.839)" },
			            { "T type", "0.455 (0.355-0.555)" },
			            { "T type + emm type", "0.755 (0.665-0.845)" },
			            { "PFGE SFi68", "0.756 (0.679-0.833)" },
		            }
	            },
            };



        Dictionary<string, Dictionary<string, string>> _methodWallace = new Dictionary<string, Dictionary<string, string>> 
            {
	            {
	            "emm type", new Dictionary<string, string>
		            {
			            { "T type", "0.696 (0.623-0.770)" },
			            { "T type + emm type", "1.000 (1.000-1.000)" },
			            { "PFGE SFi68", "0.848 (0.799-0.897)" },
			            { "PFGE Sma80", "0.951 (0.933-0.970)" },
		            }
	            },
	            {
	            "T type", new Dictionary<string, string>
		            {
			            { "emm type", "0.861 (0.809-0.912)" },
			            { "T type + emm type", "1.000 (1.000-1.000)" },
			            { "PFGE SFi68", "0.764 (0.709-0.819)" },
			            { "PFGE Sma80", "0.818 (0.763-0.873)" },
		            }
	            },
	            {
	            "T type + emm type", new Dictionary<string, string>
		            {
			            { "emm type", "0.861 (0.809-0.912)" },
			            { "T type", "0.696 (0.623-0.770)" },
			            { "PFGE SFi68", "0.731 (0.667-0.794)" },
			            { "PFGE Sma80", "0.817 (0.762-0.872)" },
		            }
	            },
	            {
	            "PFGE SFi68", new Dictionary<string, string>
		            {
			            { "emm type", "0.724 (0.649-0.800)" },
			            { "T type", "0.528 (0.448-0.608)" },
			            { "T type + emm type", "0.725 (0.644-0.806)" },
			            { "PFGE Sma80", "0.812 (0.745-0.879)" },
		            }
	            },
	            {
	            "PFGE Sma80", new Dictionary<string, string>
		            {
			            { "emm type", "0.803 (0.736-0.870)" },
			            { "T type", "0.559 (0.478-0.640)" },
			            { "T type + emm type", "0.802 (0.729-0.874)" },
			            { "PFGE SFi68", "0.802 (0.740-0.865)" },
		            }
	            },
            };

        Dictionary<string, double> _methodWallaceI = new Dictionary<string, double>
            {
	            {
	            "emm type", 0.225
	            },
	            {
	            "T type", 0.279
	            },
	            {
	            "T type + emm type", 0.194
	            },
	            {
	            "PFGE SFi68", 0.193
	            },
	            {
	            "PFGE Sma80", 0.19
	            },
            };
        #endregion


        public UnitTestComparePartitions()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext _testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return _testContextInstance;
            }
            set
            {
                _testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestPartitionCongruenceMetricCalculations()
        {
            var decimalRegex = new Regex(@"\d\.\d+", RegexOptions.Compiled);
            foreach (var pA in _testMethods)
            {
                List<string> methodAValues = pA.Value;
                string methodA = pA.Key;
                foreach (var pB in _testMethods)
                {
                    List<string> methodBValues = pB.Value;
                    string methodB = pB.Key;
                    if (methodA == methodB)
                        continue;
                    var cp = new ComparePartitions(methodAValues, methodBValues, methodA, methodB);
                    Assert.AreEqual(_methodSimpsons[methodA], cp.SimpsonsA, 0.001);
                    Assert.AreEqual(_methodSimpsonsLower[methodA], cp.SimpsonsALower, 0.001);
                    Assert.AreEqual(_methodSimpsonsUpper[methodA], cp.SimpsonsAUpper, 0.001);
                    Assert.AreEqual(_methodSimpsons[methodB], cp.SimpsonsB, 0.001);
                    Assert.AreEqual(_methodSimpsonsLower[methodB], cp.SimpsonsBLower, 0.001);
                    Assert.AreEqual(_methodSimpsonsUpper[methodB], cp.SimpsonsBUpper, 0.001);
                    Assert.AreEqual(_methodAdjustedRand[methodA][methodB], cp.AdjustedRand, 0.001);
                    Assert.AreEqual(_methodRand[methodA][methodB], cp.Rand, 0.001);

                    Assert.AreEqual(_methodWallaceI[methodA], cp.WallaceIa, 0.001);
                    Assert.AreEqual(_methodWallaceI[methodB], cp.WallaceIb, 0.001);

                    var awAvsB = _methodAdjustedWallace[methodB][methodA];
                    MatchCollection matches = decimalRegex.Matches(awAvsB);

                    var nums = new List<double>();
                    foreach (Match match in matches)
                    {
                        string value = match.Value;
                        double d = double.Parse(value, CultureInfo.InvariantCulture);
                        nums.Add(d);
                    }

                    Assert.AreEqual(nums[0], cp.AdjustedWallaceAvsB, 0.001);
                    Assert.AreEqual(nums[1], cp.AdjustedWallaceAvsBLower, 0.001);
                    Assert.AreEqual(nums[2], cp.AdjustedWallaceAvsBUpper, 0.001);

                    // TODO: find out why the Wallace confidence interval calculation is off vs the Comparing Partitions site
                    //var awBvsA = _methodWallace[methodA][methodB];
                    //matches = decimalRegex.Matches(awBvsA);

                    //nums = new List<double>();
                    //foreach (Match match in matches)
                    //{
                    //    string value = match.Value;
                    //    double d = double.Parse(value);
                    //    nums.Add(d);
                    //}

                    //Assert.AreEqual(nums[0], cp.WallaceBvsA, 0.001);
                    //Assert.AreEqual(nums[1], cp.WallaceBvsALower, 0.001);
                    //Assert.AreEqual(nums[2], cp.WallaceBvsAUpper, 0.001);


                    //Assert.AreEqual(awAvsB, string.Format("{0:0.000} ({1:0.000}-{2:0.000})", nums[0], nums[1], nums[2]));



                }
            }
        }
    }
}
