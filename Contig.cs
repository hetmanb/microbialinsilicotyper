﻿using System;

namespace MicrobialInSilicoTyper
{
    [Serializable]
    public class Contig
    {
        private readonly string _header;
        private readonly int _index;

        public ContigCollection MultifastaFile
        {
            get { return _multifastaFile; }
        }

        public string Header { get { return _header; } }
        private string _sequence;
        public string Sequence { get { return _sequence; } }

        private readonly ContigCollection _multifastaFile;
        public int Index { get { return _index; } }

        public Contig(string h, int index,  string seq, ContigCollection multifastaFile)
        {
            _header = h;
            _index = index;
            _sequence = seq;
            _multifastaFile = multifastaFile;
        }

        public void ClearSequence()
        {
            _sequence = null;
        }
    }
}
