﻿namespace MicrobialInSilicoTyper
{
    public class SNPMarker: IMarker
    {
        public string Name { get; set; }

        public string TestName { get; set; }

        private TestType _testType;
        public TestType ResultType
        {
            get { return _testType; }
            set { _testType = value; }
        }

        public string ProbeSequence { get; set; }
        public double BlastIdentity { get; set; }
        
        public SNPMarker(
            string testName,
            string name,
            string probeSequence,
            double blastIdentity)
        {
            _testType = TestType.SNP;
            TestName = testName;
            Name = name;
            ProbeSequence = probeSequence;
            BlastIdentity = blastIdentity;
        }

        public Marker GetMarker()
        {
            var p = new Marker(
                name: Name, 
                testName: TestName, 
                testType: _testType, 
                fprimer: ProbeSequence, 
                rprimer: string.Empty, 
                ampliconSize: -1, 
                ampliconRange: BlastIdentity, 
                allelicDatabaseFilename: string.Empty, 
                repeatSize: -1
                );
            return p;
        }
    }
}