﻿namespace MicrobialInSilicoTyper
{
    partial class TestTypeSelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radPCR = new System.Windows.Forms.RadioButton();
            this.radProbe = new System.Windows.Forms.RadioButton();
            this.radAllelic = new System.Windows.Forms.RadioButton();
            this.radVNTR = new System.Windows.Forms.RadioButton();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.radSNP = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // radPCR
            // 
            this.radPCR.AutoSize = true;
            this.radPCR.Checked = true;
            this.radPCR.Location = new System.Drawing.Point(12, 12);
            this.radPCR.Name = "radPCR";
            this.radPCR.Size = new System.Drawing.Size(47, 17);
            this.radPCR.TabIndex = 0;
            this.radPCR.TabStop = true;
            this.radPCR.Text = "PCR";
            this.radPCR.UseVisualStyleBackColor = true;
            // 
            // radProbe
            // 
            this.radProbe.AutoSize = true;
            this.radProbe.Location = new System.Drawing.Point(12, 35);
            this.radProbe.Name = "radProbe";
            this.radProbe.Size = new System.Drawing.Size(53, 17);
            this.radProbe.TabIndex = 1;
            this.radProbe.Text = "Probe";
            this.radProbe.UseVisualStyleBackColor = true;
            // 
            // radAllelic
            // 
            this.radAllelic.AutoSize = true;
            this.radAllelic.Location = new System.Drawing.Point(74, 12);
            this.radAllelic.Name = "radAllelic";
            this.radAllelic.Size = new System.Drawing.Size(52, 17);
            this.radAllelic.TabIndex = 2;
            this.radAllelic.Text = "Allelic";
            this.radAllelic.UseVisualStyleBackColor = true;
            // 
            // radVNTR
            // 
            this.radVNTR.AutoSize = true;
            this.radVNTR.Location = new System.Drawing.Point(74, 35);
            this.radVNTR.Name = "radVNTR";
            this.radVNTR.Size = new System.Drawing.Size(55, 17);
            this.radVNTR.TabIndex = 3;
            this.radVNTR.Text = "VNTR";
            this.radVNTR.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(12, 78);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(52, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(74, 78);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(52, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // radSNP
            // 
            this.radSNP.AutoSize = true;
            this.radSNP.Location = new System.Drawing.Point(12, 58);
            this.radSNP.Name = "radSNP";
            this.radSNP.Size = new System.Drawing.Size(78, 17);
            this.radSNP.TabIndex = 6;
            this.radSNP.Text = "SNP Probe";
            this.radSNP.UseVisualStyleBackColor = true;
            // 
            // TestTypeSelectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(134, 107);
            this.Controls.Add(this.radSNP);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.radVNTR);
            this.Controls.Add(this.radAllelic);
            this.Controls.Add(this.radProbe);
            this.Controls.Add(this.radPCR);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "TestTypeSelectionForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "New Test Type";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radPCR;
        private System.Windows.Forms.RadioButton radProbe;
        private System.Windows.Forms.RadioButton radAllelic;
        private System.Windows.Forms.RadioButton radVNTR;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.RadioButton radSNP;
    }
}